#!/bin/bash

# Make sure PWD is project root
SCRIPT_PARENT_DIR=$(readlink -f "$(dirname "${BASH_SOURCE[0]}")/..")
if [ "$(pwd)" != "${SCRIPT_PARENT_DIR}" ]; then
  echo "Error: Due to relative paths used in this script,"
  echo "you must run it from the project root directory!"
  echo ""
  echo "cd \"${SCRIPT_PARENT_DIR}\""
  echo "GITLAB_USERNAME=${GITLAB_USERNAME:-<your-username>} scripts/build-push-pytango-xrt-image.sh"
  exit 1
fi

if [ -z "${GITLAB_USERNAME}" ]; then
  echo "Error: Need your GitLab username in env. var. GITLAB_USERNAME"
  exit 1
fi

if [ ! -f .gitlab_token ]; then
  echo "Error: Need a GitLab token with read_registry & write_registry permissions in the file .gitlab_token"
  exit 1
fi

BUILD_CONTEXT_DIR=images/pytango-xrt/build_context
# default to docker (as per .make at time of writing), allow user override
OCI_BUILDER="${OCI_BUILDER:-docker}"
${OCI_BUILDER} login -u "$GITLAB_USERNAME" registry.gitlab.com --password-stdin < .gitlab_token

UBUNTU_VERSION="22.04"
PYTANGO_TAG="9.5.0"  # ska-tango-images-pytango-runtime tag
XRT_VERSION="202220.2.14.354"
XRT_PACKAGE="xrt_${XRT_VERSION}_${UBUNTU_VERSION}-amd64-xrt.deb"
XRT_URL="https://www.xilinx.com/bin/public/amdOpenDownload?filename=${XRT_PACKAGE}"
XRT_FILE="${BUILD_CONTEXT_DIR}/${XRT_PACKAGE}"
if [ ! -f ${XRT_FILE} ]; then
  echo "Download this file:"
  echo -e "\t${XRT_URL}"
  echo "Save it as '$XRT_PACKAGE' in the build context directory:"
  echo "  ${BUILD_CONTEXT_DIR}"
  read -r -p "Press Enter to continue (Ctrl-C to cancel)"
fi
if [ ! -f "${XRT_FILE}" ]; then
  echo "Error: File not found: $XRT_PACKAGE"
fi

# we need to trick `make oci-build` into honouring OCI_BUILD_ADDITIONAL_TAGS
# by setting CAR_OCI_REGISTRY_HOST for some reason
CAR_OCI_REGISTRY_HOST=registry.gitlab.com/ska-telescope/low-cbf/ska-low-cbf-proc

TAG="${PYTANGO_TAG}-${XRT_VERSION}"

# skip push because we don't want to use the default tag (project version)
DEBUG_DIRTY=1 OCI_IMAGE=pytango-xrt OCI_BUILD_ADDITIONAL_TAGS="${TAG}" \
  OCI_BUILD_ADDITIONAL_ARGS="--build-arg XRT_PACKAGE=${XRT_PACKAGE}" \
  CAR_OCI_REGISTRY_HOST=${CAR_OCI_REGISTRY_HOST} OCI_SKIP_PUSH=true \
  OCI_BUILDER=${OCI_BUILDER} OCI_IMAGE_BUILD_CONTEXT="${BUILD_CONTEXT_DIR}" \
  make oci-build

# tag is applied in `make oci-build` (but so are other tags we do not want to publish)
IMAGE_TAG="registry.gitlab.com/ska-telescope/low-cbf/ska-low-cbf-proc/pytango-xrt:${TAG}"
echo "Pushing"
${OCI_BUILDER} push "${IMAGE_TAG}"
