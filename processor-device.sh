#!/usr/bin/env bash

# script to help the helm template for device servers.
# we have to give it a command to run the device server,
# the template will add the instance parameter

SCRIPT_DIR=`dirname ${BASH_SOURCE[0]}`
echo "LowCbfProcessor launch script called with arguments: $@"

FPGA_DRIVER=AMI
if [ -f /opt/xilinx/xrt/setup.sh ]; then
  # set up Xilinx XRT driver
  source /opt/xilinx/xrt/setup.sh
  FPGA_DRIVER=XRT
fi

# find serial number - the hard way

if [ "$FPGA_DRIVER" = "XRT" ]; then
  # sed command assumes exactly one FPGA present
  # finds PCIe BDF which is enclosed in square brackets, at or near the start of a line
  MGMT_BDF=$(xbmgmt examine | sed -n "s/^ *\[\([0-9a-f:\.]*\)\].*/\1/p")
  if [ -n "$MGMT_BDF" ] && [ $(wc -w <<< "$MGMT_BDF") == 1 ] ; then
    MGMT_DIR=$(find /sys/devices ! -readable -prune -o -type d -name "$MGMT_BDF" -print)
    SERIAL_FILE=$(find "$MGMT_DIR" -name serial_num)
    SERIAL_NUM=$(cat "$SERIAL_FILE")
    export SERIAL_NUM
  else
    if [ -z "$MGMT_BDF" ]; then
      echo "Error: Did not find any FPGA"
    else
      echo "Error: Found multiple FPGAs: $MGMT_BDF"
    fi
  fi
else
  # AMI driver
  # we assume only one FPGA present
  # FIXME - this won't work later!
  BDF=$(ami_tool overview | tail -n 2 | head -n 1 | cut -d ' ' -f 1)  # e.g. 09:00.0
  # find directory corresponding to BDF e.g. 0000:09:00.0
  SYS_DIR=$(find /sys/devices ! -readable -prune -o -type d -name "*$BDF" -print)
  SERIAL_FILE="$SYS_DIR/board_serial"
  SERIAL_NUM=$(cat "$SERIAL_FILE")
  export SERIAL_NUM
fi
echo "FPGA driver: $FPGA_DRIVER"
echo "FPGA serial number: $SERIAL_NUM"
# run the device server with parameters (e.g. instance) we were given
if [ "$FPGA_DRIVER" = "AMI" ]; then
  # FIXME - horrible security, there is bound to be a better way to access ami_tool
  sudo LowCbfProcessor "$@"
else
  LowCbfProcessor "$@"
fi
