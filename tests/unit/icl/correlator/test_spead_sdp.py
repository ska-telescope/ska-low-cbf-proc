# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.
import math

import pytest
from numpy.testing import assert_allclose
from ska_low_cbf_fpga import ArgsFieldInfo

from ska_low_cbf_proc.icl.correlator.spead_sdp import (
    SpeadSdp,
    binary_fraction,
    first_visibility_freq,
    float_from_binary_fraction,
    heap_counter,
)

registers = {
    "spead_init_hz_base_int": ArgsFieldInfo(address=1, length=1),
    "spead_init_hz_base_frac": ArgsFieldInfo(address=2, length=1),
    "spead_init_hz_incr_int": ArgsFieldInfo(address=3, length=1),
    "spead_init_hz_incr_frac": ArgsFieldInfo(address=4, length=1),
}


@pytest.mark.parametrize(
    "x, expected_int, expected_frac",
    [(1, 1, 0), (2.5, 2, 0x8000_0000), (333_333.75, 333_333, 0xC000_0000)],
)
class TestBinaryFixedPoint:
    """Prove our conversions to/from fixed point are correct with simple cases."""

    def test_binary_fraction(self, x, expected_int, expected_frac):
        assert binary_fraction(x) == (expected_int, expected_frac)

    def test_float(self, x, expected_int, expected_frac):
        assert float_from_binary_fraction(expected_int, expected_frac) == x


class TestSpeadSdp:
    def test_init(self, mock_driver):
        SpeadSdp(mock_driver, registers)

    @pytest.mark.parametrize(
        "prop", ["init_hertz_base", "init_hertz_increment"]
    )
    def test_init_hz_properties(self, mock_driver, prop):
        """Test fixed point binary conversion/register round-trip."""
        sdp = SpeadSdp(mock_driver, registers)
        value = 226.056_134_259_259
        setattr(sdp, prop, value)
        # we want accuracy to 1ns
        assert_allclose(getattr(sdp, prop).value, value, rtol=1e-9)


def test_first_visibility_freq():
    """Check visibility frequency (Hertz) calculation."""
    # We think this is the right answer for 24 fine channels,
    # SPS freq. ID (coarse channel) 100
    assert_allclose(first_visibility_freq(100, 24), 77736974.64554398)


@pytest.mark.parametrize(
    "subarray, beam, vis_freq, integration",
    [
        (1, 1, 1, None),
        (16, 511, 1 << 16, None),
        (1, 1, 1, 1),
        (16, 511, 1 << 16, 0),
        (16, 511, 1 << 16, 1 << 15),
    ],
)
def test_heap_counter_size(subarray, beam, vis_freq, integration):
    """Confirm that Heap Counter fits within required space."""
    counter = heap_counter(subarray, beam, vis_freq, integration)
    n_bits = math.ceil(math.log2(counter))
    if integration is None:
        # without integration ID we need to fit in 32 bits
        assert n_bits <= 32
    else:
        # full heap counter must fit in 48 bits
        assert n_bits <= 48


@pytest.mark.parametrize(
    "subarray, beam, vis_freq, integration",
    [
        (32, 1, 1, None),
        (1, 512, 1, None),
        (1, 1, 1 << 17, None),
        (32, 1, 1, 1),
        (1, 512, 1, 1),
        (1, 1, 1 << 17, 1),
        (1, 1, 1, 1 << 16),
    ],
)
def test_heap_counter_out_of_range(subarray, beam, vis_freq, integration):
    with pytest.raises(Exception):
        heap_counter(subarray, beam, vis_freq, integration)
