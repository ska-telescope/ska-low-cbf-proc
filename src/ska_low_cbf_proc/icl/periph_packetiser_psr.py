# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.


import numpy as np
from ska_low_cbf_fpga import FpgaPeripheral


class FpgaPacketiserPsr:
    """
    An interface to PSR Packetiser peripherals
        **** FPGA Peripheral name: packetiser ****

    Each signal processing pipeline has a packetiser peripheral that contains
    - control register
    - statistics registers
    - RAM block
    RAM contents by 32-bit word address:
        0-45      (46 words) Ethernet, UDP, and PSR header values
        512-527   (16 words) PST beam numbers for beams 0..16 from pipeline
        2048-4095 (2k words) lo 32-bits PSR "first frequency" (index see below)
        4096-6134 (2k words) hi 32-bits PSR "first frequency" (index see below)
        6144-8191 (2k words) PSR "first channel" (index see below)

        2kword tables are indexed by block of channels that fits into a packet
        - PST packets have 24 PST frequency chans per packet
        - PSS packets have 54 PSS frequency chans per packet
        EG
        - lowest 24/54 channels use index 0
        - next higher 24/54 channels use index 1, etc
    """

    CONFIG_TABLE_OFFSET = 0
    BEAM_NUMBERS_TABLE_OFFSET = 512
    FIRST_FRQ_MAP_LO_OFFSET = 2 * 1024
    FIRST_FRQ_MAP_HI_OFFSET = 4 * 1024
    FIRST_CHAN_NUMBER_OFFSET = 6 * 1024

    MAX_BEAMS = 16
    MAX_PSR_CHANS = 2048

    def __init__(self, fpga_periph: FpgaPeripheral, logger):
        self.regs = fpga_periph  # Access to FPGA peripheral registers
        self.logger = logger

    def disable_packetiser(self) -> None:
        """
        Disable packetiser
        b3, b2, b1, b0 are "defaults", "Num_test_runs", "test_mode_en", "enable"
        """
        self.regs.control_vector.value = 0x0

    def prgm_packetiser_first_freqs(
        self, words_lo: list[int], words_hi: list[int]
    ) -> None:
        """
        Write new VCT-to-freq mapping tables for a pipeline

        :param words_lo: list of 32-bit integers - lo 32-bits of first freq
        :param words_hi: list of 32-bit integers - hi 32-bits of first freq
        """
        assert (
            len(words_lo) <= self.MAX_PSR_CHANS
        ), f"{len(words_lo)} chans, but max is {self.MAX_PSR_CHANS}"
        assert (
            len(words_hi) <= self.MAX_PSR_CHANS
        ), f"{len(words_hi)} chans, but max is {self.MAX_PSR_CHANS}"

        np_words_lo = np.asarray(words_lo, dtype=np.uint32)
        start = self.FIRST_FRQ_MAP_LO_OFFSET
        end = start + len(words_lo)
        self.regs.data[start:end] = np_words_lo

        np_words_hi = np.asarray(words_hi, dtype=np.uint32)
        start = self.FIRST_FRQ_MAP_HI_OFFSET
        end = start + len(words_lo)
        self.regs.data[start:end] = np_words_hi

    def prgm_packetiser_beam_nos(self, datablob: list[int]) -> None:
        """
        write new packetiser beam-numbers table (16 values) for a pipeline
        :param datablob: list of beam numbers for each beam from the pipeline
        """
        num_beams = len(datablob)
        assert (
            num_beams <= self.MAX_BEAMS
        ), f"Maximum {self.MAX_BEAMS}, got {num_beams}"

        data_as_np = np.asarray(datablob, dtype=np.uint32)

        start = self.BEAM_NUMBERS_TABLE_OFFSET
        end = start + len(datablob)
        self.regs.data[start:end] = data_as_np

    def prgm_packetiser_first_chans(self, chan_nos: list[int]) -> None:
        """
        write new packetiser first-channel-no table (16 values) for a pipeline
        :param idx: which signal processing pipeline to update [0,1,2]
        """
        assert (
            len(chan_nos) <= self.MAX_PSR_CHANS
        ), f"{len(chan_nos)} chans, but max is {self.MAX_PSR_CHANS}"

        np_chan_nos = np.asarray(chan_nos, dtype=np.uint32)
        start = self.FIRST_CHAN_NUMBER_OFFSET
        end = start + len(chan_nos)
        self.regs.data[start:end] = np_chan_nos

    def disable_and_prgm_packetiser(self, hdr_data: list[int]) -> None:
        """
        Disable packetiser and write new packetiser header info
        :param hdr_data: IP, UDP, PSR header info
        """
        self.disable_packetiser()

        np_hdr_data = np.asarray(hdr_data, dtype=np.uint32)
        start = self.CONFIG_TABLE_OFFSET
        end = start + len(hdr_data)
        self.regs.data[start:end] = np_hdr_data

    def packetiser_enable(self, enable: bool) -> None:
        """
        Enable or disable packetiser.
        FPGA requires a rising edge to enable & load new parameters

        Note: this clears any default/packetgen settings

        :param enable: enable/disable packetiser and reload of params
        """
        self.regs.control_vector.value = 0x0
        if enable:
            self.regs.control_vector.value = 0x1

    def get_num_valid_pkts(self) -> int:
        """
        Read how many valid packets the signal processing produced
        """
        return self.regs.stats_packets_rx_sig_proc_valid.value
