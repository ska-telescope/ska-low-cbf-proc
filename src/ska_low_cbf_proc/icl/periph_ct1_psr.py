# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.


import numpy as np
from ska_low_cbf_fpga import FpgaPeripheral


class FpgaCt1Psr:
    """
    An interface to PSS & PST first corner turn (CT1) peripherals
        **** FPGA Peripheral Name: "CT1" ****
    CT1 includes the RAM for beam polynomial coefficients, contents:
        20kword buffer0 stn_dly_coeffs: 1024 Vchans * 20 Words/entry
        20kword buffer1 stn_dly_coeffs: 1024 Vchans * 20 Words/entry
    RAM entries for one Vchan are:
        c0, c1, c2, c3, c4, c5 (the six polynomial coefficients)
        sky_ghz, offset_secs, pol_Y_offset_ns, valid_time+valid_bit
    """

    POLY_BUF_OFFSET = 0
    POLY_BUF_SIZE = 20 * 1024  # 20 words per vchan in FPGA polynomial buffer

    def __init__(self, fpga_periph: FpgaPeripheral, logger):
        self.regs = fpga_periph  # Access to FPGA peripheral registers
        self.logger = logger

    def ct2_temporary_reset(self) -> None:
        """
        Apply full reset to corner turn 2 to reset its logic and
        pick up any change to channels/stations
        (CT2 reset is located in CT1 registers)
        """
        self.regs.full_reset.value = 0x1
        self.regs.full_reset.value = 0x0

    def read_ct1_count(self):
        """
        Read current frame number being written to CT1.
        Filterbank processing may be up to 2 frames behind this
        """
        low = self.regs.frame_count_low.value
        hi = self.regs.frame_count_high.value

        return (hi << 32) + low

    def update_beam_delay_polys(self, buffer_pair: list) -> None:
        """
        Write delay polynomial data into firmware registers

        We write both buffers at once. (For one of the buffers the data written
        is expected to be the same as already in the buffer)

        :param buffer_pair: Two-element list, each element a 20kword list
        of integers to be written to FPGA delay polynomial buffers as uint32
        """
        for buf_no, buffer in enumerate(buffer_pair):
            start = self.POLY_BUF_OFFSET + self.POLY_BUF_SIZE * buf_no
            end = start + self.POLY_BUF_SIZE
            self.regs.data[start:end] = np.asarray(buffer, dtype=np.uint32)
