# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""
Functions to extract PSS information from allocator attribute data

This file exists to confine references to specific field names
in the internal_alveo and internal_subarray attributes to a single file
"""

from dataclasses import dataclass


def get_subarray_lists(subarray_info):
    """
    From global subarray information (published by allocator), extract
    - sorted list of all subarray_ids (configured subarrays, scanning or not)
    - sorted list of scanning subarray_ids (subset of above)
    """
    all_subarray_ids = set()
    scanning_subs = set()
    for (
        subarray_id_txt,
        subarray_description,
    ) in subarray_info.items():
        subarray_id = int(subarray_id_txt)
        all_subarray_ids.add(subarray_id)

        if subarray_description["scanning"]:
            scanning_subs.add(subarray_id)
    return sorted(list(all_subarray_ids)), sorted(list(scanning_subs))


def stn_order(sstns: list) -> list:
    """get station/substation order for VCT"""
    # Get a number larger than max substation id
    substation_ids = [sstn for _, sstn in sstns]
    max_sstn = max(substation_ids) + 1
    # make single number representing relative order for each entry
    rel_rank = [stn * max_sstn + sstn for stn, sstn in sstns]

    temp = sorted(rel_rank)
    order = [temp.index(i) for i in rel_rank]
    return order


@dataclass
class SigProcParams:
    """
    Structure describing signal processing parameters for one PSS pipeline.
    It contains only information that is constant for the duration of a subarray
    configuration. It does not (cannot) include scan_id or scanning status which
    change from one scan to another during a subarray configuration
    """

    subarray_id: int
    """subarray ID"""
    stn_bm_id: int
    """station beam id"""
    freq_ids: list[int]
    """sorted list of frequency IDs in station beam"""
    stn_ids: list[tuple]
    """unsorted list of (station, substation) tuples for station beam"""
    stn_vct_order: list
    """VCT order of each station in stn_ids list"""
    stn_bm_dly_src: str
    """Station beam delay polymial source, Tango URI"""
    pss_bm_ids: list[int]
    """List of PSS beam IDs formed from station beam"""
    pss_dly_srcs: list[str]
    """List of delay polynomial sources (Tango URI) for each PSS beam"""
    jones_srcs: list[str]
    """List of Jones sources (Tango URI) for each PSS beam"""
    stn_weights: list[list[float] | None]
    """List of station weight lists for each PSS beam"""


@dataclass
class PssDest:
    """Structure describing a server destination for PSS beam packets"""

    ip_addr: str
    """IP address to send to eg '10.1.2.3' """
    udp_port: int
    """ UDP port number to send to"""
    start_chan: int
    """ First PSS channel sent to this host"""
    num_chans: int
    """ Number of subsequent channels sent to this host"""


@dataclass
class PssBeam:
    """Structure describing a PSS beam"""

    pss_beam_id: int
    """PSS beam_ID (expect [1..16])"""
    delay_poly_uri: str
    """Tango subscription URI for PSS delays"""
    jones_uri: str
    """ Tango subscription URI for Jones matrix"""
    server_dests: list
    """ List of PssDest objects (expect 1 or 2 only)"""


def get_station_beam_pss_info(intl_subarray, subarray_id, beam_id):
    """
    From subarray info published by allocator internal_subarray attribute,
    extract info about the PSS beams in for a particular subarray station-beam

    :param intl_subarray: the value published by the allocator's
    internal_subarray attribute
    :param subarray_id: ID of the subarray
    :param beam_id: station_beam ID
    """
    subarray_pss_beams = intl_subarray[str(subarray_id)]["search_beams"][
        "beams"
    ]
    stn_bm_pss_beams = []
    for beam in subarray_pss_beams:
        if beam["stn_beam_id"] != beam_id:
            continue
        server_dests = []
        for itm in beam["destinations"]:
            dest = PssDest(
                itm["data_host"],
                itm["data_port"],
                itm["start_channel"],
                itm["num_channels"],
            )
            server_dests.append(dest)
        bm = PssBeam(
            beam["pss_beam_id"],
            beam["delay_poly"],
            beam["jones"],
            server_dests,
        )
        stn_bm_pss_beams.append(bm)
    return stn_bm_pss_beams


def get_internal_scan_id(subarray_info, subarray_id):
    """
    From internal_subarray attribute value published by the allocator,
    extract scan_id (if any) for selected subarray_id

    :param subarray_info: the value published by the allocator's
    internal_subarray attribute
    :param subarray_id: ID of the subarray being scanned

    [Note: scan_id can't be part of the SigProcParams returned by
    get_pipeline_params below, since the latter is called when a subarray is
    configured, but scan_id changes after configuration from one scan to
    another. scan_id has to be extracted each time from subarray_info]

    """
    scan_id = subarray_info[str(subarray_id)].get("scan_id", 0)
    return scan_id


def get_internal_subarray_id(intl_alveo, idx):
    """
    From internal_alveo attribute data published by allocator, extract
    the subarray ID associated with a pipeline identified by its index [0..2]

    :param intl_alveo: the value published by the allocator's
    internal_alveo attribute

    [Note: subarray_id has to be extracted from the internal_alveo value
    published by the allocator - to see if it has changed. It cannot be
    obtained from SigProcParams that reflect existing configuration]
    """
    if idx > len(intl_alveo):
        return None
    # Allow for the entry to be an empty dictionary if pipeline is unused
    subarray_id = intl_alveo[idx].get("subarray_id", None)
    return subarray_id


def subarray_beams_as_dict(subarray_id, intl_subarray) -> dict:
    """
    Extract description of a selected subarray from the global subarray info

    :param subarray_id: the subarray ID
    :param intl_subarray: value of the allocator internal_subarray attribute
    :return: dict describing the subarray station beams and their pss beams
    """
    subarray_info = intl_subarray[str(subarray_id)]

    stn_beams = {}
    # Add station beams to dict, key=station_beam_id
    for bm in subarray_info["stn_beams"]:
        stn_beams[bm["beam_id"]] = {
            "freq_ids": bm["freq_ids"],
            "delay_poly": bm["delay_poly"],
            "search_beams": {},  # Data added in loop below
        }
    # Add timing beams into appropriate station beam
    for bm in subarray_info["search_beams"]["beams"]:
        stn_bm_id = bm["stn_beam_id"]
        if stn_bm_id not in stn_beams:
            # User specified a pss beam in an unknown station beam. Ignore
            continue
        stn_beams[stn_bm_id]["search_beams"][bm["pss_beam_id"]] = {
            "pss_poly": bm["delay_poly"],
            "jones": bm["jones"],
            "destinations": bm["destinations"],
            "stn_weights": bm["stn_weights"],
        }
    return stn_beams


def get_pipeline_params(intl_alveo, intl_subarray, idx) -> tuple:
    """
    From internal_alveo and subarray attributes extract info describing
    the processing in a PSS pipeline

    :param intl_alveo: the internal_alveo definition for this FPGA
    :param intl_subarray: subarray descriptions
    :param idx: which internal_alveo pipeline we want info about
    :return: tuple of data about this pipeline's processing (see below)
    """
    # parameters we can get from the pipeline def (internal_alveo)
    subarray_id = intl_alveo[idx]["subarray_id"]
    stn_bm_id = intl_alveo[idx]["stn_bm_id"]  # stn beam for this pipeline
    freq_ids = intl_alveo[idx]["freq_ids"]  # freqs this FPGA processes
    sstns = intl_alveo[idx]["stns"]  # same info exists in subarray def
    stn_vct_order = stn_order(sstns)
    pss_bm_ids = intl_alveo[idx]["pss_bm_ids"]

    pss_dly_srcs = [None] * len(pss_bm_ids)
    jones_srcs = [None] * len(pss_bm_ids)
    stn_weights = [None] * len(pss_bm_ids)
    # parameters we need to get from subarray def (internal_subarray)
    stn_beams = subarray_beams_as_dict(subarray_id, intl_subarray)
    assert (
        stn_bm_id in stn_beams
    ), f"Error in subarray.configure PSS: no stn_beam_id={stn_bm_id}"
    stn_beam_dly_src = stn_beams[stn_bm_id]["delay_poly"]
    for idx, pss_bm_id in enumerate(pss_bm_ids):
        assert (
            pss_bm_id in stn_beams[stn_bm_id]["search_beams"]
        ), f"Error in subarray.configure PSS: bad pss_beam_id={pss_bm_id}"
        timing_beam = stn_beams[stn_bm_id]["search_beams"][pss_bm_id]
        pss_dly_srcs[idx] = timing_beam["pss_poly"]
        jones_srcs[idx] = timing_beam["jones"]
        stn_weights[idx] = timing_beam["stn_weights"]

    sig_proc_params = SigProcParams(
        subarray_id,
        stn_bm_id,  # station-beam this FPGA pipile processes
        freq_ids,  # sorted list of frequency IDs this pipeline is processing
        sstns,  # unsorted list of (station, sub-station) tuples
        stn_vct_order,  # vct order of each item in sstns above
        stn_beam_dly_src,  # station-beam delay poly source
        pss_bm_ids,  # list of pss beam_ids
        pss_dly_srcs,  # list of pss delay poly sources, one per beam_id
        jones_srcs,  # list of pss jones sources, one per beam_id
        stn_weights,  # list of station weights, one list per bam
    )
    return sig_proc_params
