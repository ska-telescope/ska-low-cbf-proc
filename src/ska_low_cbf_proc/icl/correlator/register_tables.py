# -*- coding: utf-8 -*-
#
# Copyright (c) 2025 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.
"""
Calculate values for register tables in Correlator FPGA.

Virtual channel table
Virtual channel demap table
Subarray beam table

"""
import math
from dataclasses import dataclass


@dataclass
class SbtEntry:
    """Structure holding data for one entry row of subarray_beam_table"""

    stn_bm: int
    first_coarse: int
    first_fine: int
    num_fine: int
    t_integ: int
    fine_ch_integ: int
    mxc: int


@dataclass
class DemapEntry:
    """Structure holding data for one row of VC Demap table"""

    freq_id: int
    vc_blk_order: int
    sbt_idx: int


@dataclass
class VctEntry:
    """Structure holding data for one VCT entry"""

    sa_id: int
    frq_id: int
    stn_bm: int
    stn: int
    sstn: int
    vchan: int


NUM_VC_BLKS = 256  # Entries in FPGA VC_DEMAP table
# NUM_VC_BLKS = 16  # Entries in FPGA VC_DEMAP table (debug)
STNS_PER_VC_BLK = 4

FINE_PER_COARSE = 3456
CT2_BYTES_PER_STN_CHNL = (3456 * 6 * 512) // 4
CT2_BYTES_PER_STN_FINE_CHAN = (6 * 512) // 4
CT2_MX_MEM_BYTES = 3 * 512 * 1024 * 1024
NUM_MXC = 2  # number of matrix correlators
SUBARRAY_BEAM_ENTRIES_PER_MXC = 128


def _vct_sort_ordering(vct_entry):
    """Function used to determine VCT entry ordering"""
    if vct_entry is None:
        return 0x80000000
    key = (vct_entry.sa_id & 0x1F) << 26  # 5-bit subarray_id
    key += (vct_entry.frq_id & 0x01FF) << 17  # 9-bit freq_id
    key += (vct_entry.stn_bm & 0x0F) << 13  # 4-bit beam_id
    key += (vct_entry.stn & 0x03FF) << 3  # 10-bit station_id
    key += vct_entry.sstn & 0x07  # 3-bit sub-station
    return key


class RegTables:
    """Class used to calclate Correlator FPGA internal table values"""

    def __init__(self):
        self._free_vchan_blks = list(range(NUM_VC_BLKS))
        self._subarray_stations = {}  # stn/sstn list for each active subarray
        self._subarray_sbt = {}  # station-beam table entries for subarray

    def release_vchans(self, subarray_ids: list):
        """
        Return blocks of virtual channels used by subarrays to free list.

        :param subarray_ids: list of subarray IDs to remove
        """
        for sa_id in subarray_ids:
            if sa_id not in self._subarray_sbt:
                print(f"No subarray_{sa_id} to free")
                continue
            vc_blks_to_free = []
            # accumulate vchan blocks assigned to this subarray
            for _, blks in self._subarray_sbt[sa_id]:
                vc_blks_to_free.extend(blks)
            del self._subarray_sbt[sa_id]
            del self._subarray_stations[sa_id]
            self._free_vchan_blks.extend(vc_blks_to_free)
        # nice to keep list sorted, so lowest numbers will be assigned first
        self._free_vchan_blks = sorted(self._free_vchan_blks)

    def assign_vchans(self, subarray_ids: list, cpr_regs) -> None:
        """
        Assign blocks of virtual channels for new subarrays

        :param subarray_ids: list of subarray IDs to add
        :param cbr_regs: Compressed registers list from allocator.
            Each list entry is a dict with keys:
            "sa_id"  subarray_id
            "stns"   list of tuples (station, substation)
            "sa_bm"  list of tuples representing subarray-beam table entries
        """
        if cpr_regs is None:
            return
        for sa_entry in cpr_regs:
            sa_id = sa_entry["sa_id"]
            if sa_id not in subarray_ids:
                continue
            stns = sa_entry["stns"]
            # Record this subarray's station/substation list
            self._subarray_stations[sa_id] = stns
            if sa_id not in self._subarray_sbt:
                self._subarray_sbt[sa_id] = []
            # Each subarray-beam table entry needs VCHAN blocks
            for sbt_entry in sa_entry["sa_bm"]:
                entry = SbtEntry(*sbt_entry)
                num_coarse = math.ceil(
                    (entry.first_fine + entry.num_fine - 1) / FINE_PER_COARSE
                )
                # Get some unused vchan blocks for this subarray-beam entry
                vc_blks = self._get_vchan_blks(len(stns), num_coarse)
                self._subarray_sbt[sa_id].append((entry, vc_blks))

    def get_vct(self) -> tuple:
        """
        Get VCT programming for current running subarrays plus other info

        This table has one 2-word entry per VCT item
        : return: tuple containing (VCT_programming_words, no of VCT entries,
        vchan usage summary)
        """
        vct_entries = []
        for subarray, itms in self._subarray_sbt.items():
            for itm in itms:
                sbt, vc_blks = itm
                stns = self._subarray_stations[subarray]
                num_coarse = math.ceil(
                    (sbt.first_fine + sbt.num_fine - 1) / FINE_PER_COARSE
                )
                stn_cntr = 0
                blk_cntr = 0
                for ch_no in range(0, num_coarse):
                    frq_id = sbt.first_coarse + ch_no
                    # each Freq Chan starts with new VC block
                    if stn_cntr != 0:
                        stn_cntr = 0
                        blk_cntr += 1
                    for stn, substn in stns:
                        vct_entries.append(
                            VctEntry(
                                subarray,
                                frq_id,
                                sbt.stn_bm,
                                stn,
                                substn,
                                vc_blks[blk_cntr] * STNS_PER_VC_BLK + stn_cntr,
                            )
                        )
                        stn_cntr += 1
                        if stn_cntr >= STNS_PER_VC_BLK:
                            stn_cntr = 0
                            blk_cntr += 1
        # Sort into the order that FPGA needs for its search
        vct_entries = sorted(vct_entries, key=_vct_sort_ordering)

        # Convert VCT entries in regs into hex words for FPGA
        hexvals = []
        for entry in vct_entries:
            word1 = (
                0x80000000  # Valid flag
                + ((entry.sa_id & 0x1F) << 26)
                + ((entry.frq_id & 0x01FF) << 17)
                + ((entry.stn_bm & 0x0F) << 13)
                + ((entry.stn & 0x03FF) << 3)
                + (entry.sstn & 0x07)
            )
            word2 = entry.vchan
            hexvals.append(word1)
            hexvals.append(word2)
        # Fill "invalid entries" to bring size to >"total_channels" value bec
        # total_channels determines both VCT search window and number of
        # 4-station-blocks read out of 1st corner turn. Latter is greater than
        # former when the number of stations in a subarray is not multiple of 4
        unused = NUM_VC_BLKS * STNS_PER_VC_BLK * 2 - len(hexvals)
        if unused > 0:
            hexvals.extend([0x0] * unused)

        # n_ch = zero if none used, else highest used Vchan +1
        max_used_vch = -1  # -1 so that no used vchan comes out as nch==0
        for entry in vct_entries:
            max_used_vch = max(max_used_vch, entry.vchan)
        n_ch = max_used_vch + 1

        return hexvals, n_ch, vct_entries

    def get_vc_demap(self) -> list:
        """
        Get VC Demap table programming for current running subarrays

        This table has one two-word entry per block of 4 Virtual channels
        in the Virtual Channel Table.
        Entries for unused VCblocks have MSB of the first word set to zero
        to mark as disabled
        A subarray-beam-table entry for C coarse channels and S stations will
        have C * ceil( S/4 ) entries in the VC Demap table
        """
        vct_regs = {}
        nxt_sbt_cnt = [0] * NUM_MXC  # next subarray-beam table entry for MxC
        sa_ids = sorted(list(self._subarray_sbt.keys()))  # consistent order
        for subarray in sa_ids:
            itms = self._subarray_sbt[subarray]
            for itm in itms:
                sbt, vc_blks = itm
                n_stns = len(self._subarray_stations[subarray])
                blks_per_coarse = math.ceil(n_stns / STNS_PER_VC_BLK)
                num_coarse = math.ceil(
                    (sbt.first_fine + sbt.num_fine - 1) / FINE_PER_COARSE
                )
                sbt_idx = (
                    nxt_sbt_cnt[sbt.mxc]
                    + sbt.mxc * SUBARRAY_BEAM_ENTRIES_PER_MXC
                )
                for ch_cnt in range(0, num_coarse):
                    frq_id = sbt.first_coarse + ch_cnt
                    first_blk_idx = ch_cnt * blks_per_coarse
                    for stn_blk_cnt in range(0, blks_per_coarse):
                        blk = vc_blks[first_blk_idx + stn_blk_cnt]
                        stn_order = (
                            ch_cnt * blks_per_coarse + stn_blk_cnt
                        ) * STNS_PER_VC_BLK
                        vct_regs[blk] = DemapEntry(
                            frq_id,
                            stn_order,
                            sbt_idx,
                        )
                nxt_sbt_cnt[sbt.mxc] += 1
        # Convert demap entries in regs into hex words for FPGA
        hexvals = []
        n_demap_entries = SUBARRAY_BEAM_ENTRIES_PER_MXC * NUM_MXC
        for idx in range(0, n_demap_entries):
            if idx not in vct_regs:
                word1 = 0x0  # unused entries marked invalid
                word2 = 0x0
            else:
                word1 = (
                    0x80000000
                    + ((vct_regs[idx].freq_id & 0x1FF) << 20)
                    + ((vct_regs[idx].vc_blk_order & 0x0FFF) << 8)
                    + (vct_regs[idx].sbt_idx & 0x0FF)
                )
                word2 = 0x00000000
            hexvals.append(word1)
            hexvals.append(word2)
        return hexvals

    def get_sbt(self) -> list:
        """
        Get Subarray Beam Table programming for current running subarrays

        Subarray-beam table has 4 words per entry
        """
        regs = {}
        nxt_sbt_cnt = [0] * NUM_MXC  # next subarray-beam table entry for MxC
        hbm_base = [0] * NUM_MXC  # next free HBM slot
        sa_ids = sorted(list(self._subarray_sbt.keys()))  # consistent order
        for subarray in sa_ids:
            for itm in self._subarray_sbt[subarray]:
                sbt, _ = itm
                n_stns = len(self._subarray_stations[subarray])
                vc_blks_per_coarse = math.ceil(
                    len(self._subarray_stations[subarray]) / STNS_PER_VC_BLK
                )
                hbm_len = (
                    sbt.num_fine
                    * CT2_BYTES_PER_STN_FINE_CHAN
                    * vc_blks_per_coarse
                    * STNS_PER_VC_BLK
                )
                sbt_idx = (
                    nxt_sbt_cnt[sbt.mxc]
                    + sbt.mxc * SUBARRAY_BEAM_ENTRIES_PER_MXC
                )
                regs[sbt_idx] = (hbm_base[sbt.mxc], sbt, n_stns)
                hbm_base[sbt.mxc] += hbm_len
                nxt_sbt_cnt[sbt.mxc] += 1

        # Convert Subarray-beam table entries in regs into hex words for FPGA
        hexvals = []
        for reg_no in range(0, NUM_MXC * SUBARRAY_BEAM_ENTRIES_PER_MXC):
            if reg_no not in regs:
                hexvals.extend([0x0, 0x0, 0x0, 0x0])  # unused words just zero
                continue
            hbm_base_addr, sbt, n_stns = regs[reg_no]
            word1 = ((sbt.first_coarse & 0x0FFFF) << 16) + (n_stns & 0x0FFFF)
            word2 = sbt.first_fine
            word3 = 0x0
            if sbt.t_integ == 192:  # 192 or 64 is value from allocator
                word3 = 0x40000000
            word3 += ((sbt.fine_ch_integ & 0x03F) << 24) + (
                sbt.num_fine & 0x0FFFFFF
            )
            word4 = hbm_base_addr
            hexvals.extend([word1, word2, word3, word4])
        return hexvals

    def get_sbt_info(self, num_mxcs: int):
        """
        Get info needed for packetiser

        :param num_mxcs: number of matrix correlators in the fpga
        """
        info = [[] for _ in range(0, num_mxcs)]
        sa_ids = sorted(list(self._subarray_sbt.keys()))  # consistent order
        for subarray in sa_ids:
            stns = self._subarray_stations[subarray]
            baselines = (len(stns) * (len(stns) + 1)) // 2
            for itm in self._subarray_sbt[subarray]:
                sbt, _ = itm
                itm_info = (
                    subarray,
                    sbt.stn_bm,
                    sbt.first_coarse,
                    sbt.first_fine,
                    sbt.num_fine,
                    sbt.t_integ,  # TODO remove this
                    sbt.fine_ch_integ,
                    baselines,
                )
                info[sbt.mxc].append(itm_info)
        return info

    def get_sbt_entries(self, num_mxcs):
        """
        Get number of subarray-beam-table entries needed for buffer flip

        :param num_mxcs: number of matrix correlators in the fpga
        """
        sbt_entries = [0] * num_mxcs
        sa_ids = sorted(list(self._subarray_sbt.keys()))
        for subarray in sa_ids:
            for itm in self._subarray_sbt[subarray]:
                sbt, _ = itm
                sbt_entries[sbt.mxc] += 1
        return sbt_entries

    def _get_vchan_blks(self, n_stns, n_chans) -> list[int] | None:
        """Get a free vchan blks to use for subarray"""
        n_blks = math.ceil(n_stns / STNS_PER_VC_BLK) * n_chans
        assert n_blks <= len(
            self._free_vchan_blks
        ), "No free VCHAN blocks: Allocator problem??"
        tmp = self._free_vchan_blks
        blks = tmp[0:n_blks]
        self._free_vchan_blks = tmp[n_blks:]
        return blks

    def show(self) -> None:
        """Debug print of vchan blocks etc"""
        print(f"free: {self._free_vchan_blks}")
        print(f"used: {self._subarray_sbt}")

    def check_no_vchans(self):
        """Debug check that there is nothing left allocated"""
        assert len(self._free_vchan_blks) == NUM_VC_BLKS, "not all sbts freed"
        assert len(self._subarray_sbt) == 0, "not all sbts deleted"


def vct_words_to_text(words):
    """Convert VCT programming words to human-readable text lines"""
    lines = []
    lines.append("========== VCT =============")
    lines.append(
        "idx    word_1      word_2      sa Chan StnBm  Stn SStn  VChan"
    )
    for base in range(0, len(words), 2):
        if (words[base] & 0x80000000) != 0:
            sa_id = (words[base] >> 26) & 0x01F
            frq_id = (words[base] >> 17) & 0x01FF
            stn_bm = (words[base] >> 13) & 0x0F
            stn = (words[base] >> 3) & 0x03FF
            sstn = words[base] & 0x07
            vch = words[base + 1]
            # Write out all the fields for this VCT entry
            line = f"{base//2:4d}: 0x{words[base]:08x}  0x{words[base+1]:08x}"
            line += f" |  {sa_id:2d} {frq_id:3d}    {stn_bm:2d}  {stn:3d}"
            line += f"  {sstn:3d}    {vch:3d}"
            lines.append(line)
    return lines


def demap_words_to_text(words):
    """Convert Demap Table programming words to human readable text"""
    lines = []
    lines.append("====== Demap Table ========")
    lines.append("idx    word_1      word_2      frq_id stn_order sbt_idx")
    for base in range(0, len(words), 2):
        if words[base] != 0:
            frq = (words[base] >> 20) & 0x01FF
            order = (words[base] >> 8) & 0x0FFF
            idx = words[base] & 0x0FF
            # Write out all the fields for this Demap table entry
            line = f"{base//2:3d}: 0x{words[base]:08x}  0x{words[base+1]:08x}"
            line += f" |     {frq:3d}    {order:3d}      {idx:3d}"
            lines.append(line)
    return lines


def sbt_words_to_text(words):
    """Convert Subarray-beam table programming words to human readable text"""
    txt = []
    txt.append("============ Subarray_beam Table ============")
    txt.append(
        "idx     word_1      word_2      word3       word 4    "
        "     HBM_base 1stFine  1ch    n_fine stn Int Ichn "
    )
    for base in range(0, len(words), 4):
        if words[base] != 0:
            line = f"{base//4:3d}:"
            for tmp in range(0, 4):
                line += f"  0x{words[base+tmp]:08x}"
            first_cse = (words[base] >> 16) & 0x0FFFF
            n_stn = words[base] & 0x0FFFF
            first_fine = words[base + 1]
            t_integ = "s" if (words[base + 2] & 0x40000000) == 0x0 else "L"
            ch_integ = (words[base + 2] >> 24) & 0x3F
            n_fine = words[base + 2] & 0x0FFFFFF
            hbm_base = words[base + 3]
            line += f" |   0x{hbm_base:08x} {first_fine:7d} {first_cse:4d}"
            line += f"   {n_fine:7d} {n_stn:3d}   {t_integ}  {ch_integ:3d}"
            txt.append(line)

    return txt
