# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.
"""SPEAD output to SDP FPGA Peripheral abstraction layer (ICL)"""
import math
import time
from socket import inet_aton
from typing import Dict, Optional

import numpy as np
from ska_low_cbf_fpga import FpgaPeripheral, IclField
from ska_low_cbf_fpga.args_fpga import WORD_SIZE, ArgsWordType

from ska_low_cbf_proc.icl.correlator.constants import (
    FINE_CH_PER_COARSE_CH,
    FINE_CH_PER_STANDARD_VIS_CH,
    FINE_CHANNEL_HZ,
    SEC_PER_FILTERBANK_SAMPL,
    SKA_COARSE_SPACING_HZ,
)
from ska_low_cbf_proc.icl.correlator.spead_init import get_sdp_init

# number of subarray-beam table entries handled by a SDP packetiser
PKTIZR_SBT_ENTRIES = 128


def visibility_frequency(
    freq_id, first_fine_ch, n_fine_ch, visibility_ch
) -> float:
    """
    Calculate the frequency of a visibility (Hz).

    :param freq_id: SPS frequency ID (aka coarse channel)
    :param fine_ch: First correlator fine channel ID used in the integration
    :param visibility_ch: Visibility channel ID
    :param n_fine_ch: Number of fine channels integrated in our correlation
    :return: Visibility centre frequency (Hz)
    """
    return (
        freq_id * SKA_COARSE_SPACING_HZ  # centre frequency
        # our fine frequency minus half of the total fine channels BW,
        # gives centre of fine channel
        + (first_fine_ch - FINE_CH_PER_COARSE_CH / 2) * FINE_CHANNEL_HZ
        # minus half a fine channel, gives bottom frequency of visibility channel zero
        - FINE_CHANNEL_HZ / 2
        # plus half a Correlation BW, gives centre freq. of first visibility
        + FINE_CHANNEL_HZ * n_fine_ch / 2
        # add visibility BW times visibility channel ID to get this visibility freq.
        + visibility_ch * FINE_CHANNEL_HZ * n_fine_ch
    )


def first_visibility_freq(first_freq_id: int, n_fine_ch: int) -> float:
    """
    Calculate the first visibility's centre frequency for a correlation.

    :param first_freq_id: SPS Frequency ID (aka coarse channel)
    :param n_fine_ch: Number of fine channels integrated in our
      correlation.
    :return: Base Frequency (Hz) for SPEAD init packets to SDP
    """
    return visibility_frequency(first_freq_id, 0, n_fine_ch, 0)


def binary_fraction(x) -> (int, int):
    """
    Convert a number to two words as binary fixed point.

    :returns: integer part, fractional part
    """
    int_part = int(x)
    frac_part = int((x - int_part) * 0x1_0000_0000)
    return int_part, frac_part


def float_from_binary_fraction(integer, fraction) -> float:
    """Convert two words of binary fixed point to a float."""
    return ((integer << 32) | fraction) / 0x1_0000_0000


def heap_counter(
    subarray: int,
    beam: int,
    visibility_channel: int,
    integration_id: Optional[int] = None,
) -> int:
    """
    Calculate SDP SPEAD Heap counter.

    :param subarray: 5 bits
    :param beam: 9 bits
    :param visibility_channel: Visibility Channel ID, 17 bits.
      Note: not the SPS channel/frequency.
    :param integration_id: 16 bits. If not supplied, heap counter will be truncated.
    :return: Heap counter. Full 47 bits if integration_id supplied.
      Upper 31 bits if not.
    """
    heap_string = f"{subarray:05b}{beam:09b}{visibility_channel:017b}"
    assert (
        len(heap_string) == 31
    ), "Heap Counter parameter(s) too big, can't calculate!"
    counter_without_integration = int(heap_string, 2)
    if integration_id is None:
        return counter_without_integration
    assert 0 <= integration_id <= 0xFFFF, "Invalid integration ID"
    return counter_without_integration << 16 | integration_id


def _get_vis_chans_per_sdp_host(beam_def) -> int:
    """
    From the subarray definition work out the number of visibility channels
    that a SDP server can receive.
    """
    host_list = beam_def["host"]
    # assume first SDP server can receive all ports if only one server
    if len(host_list) == 1:
        return 1_000_000  # more than low.cbf will ever generate
    # otherwise gap between first channel for two servers is the value
    ch0, _, _ = host_list[0]
    ch1, _, _ = host_list[1]

    return ch1 - ch0


def get_stn_bm_frq_no(subarray_def, stn_bm_id, freq) -> int:
    """
    From the subarray definition,
    get the list of frequency ids for a particular station beam and return
    the index of the frequency in the list (ie 0 if first freq, etc)

    :param subarray_def: dict of subarray parameters incl station beams
    :param stn_bm_id: station-beam ID
    """
    stn_bm_list = subarray_def["stn_beams"]
    for bm_descr in stn_bm_list:
        if bm_descr["beam_id"] != stn_bm_id:
            continue
        try:
            return bm_descr["freq_ids"].index(freq)
        except ValueError:
            break  # we should have found it in this bm_descr, but we didn't
    # oops didn't find anything - can't occur. software error
    raise RuntimeError("Couldn't find station beam frequency. Impossible!")


def _get_vis_output(subarray_def, stn_bm_id) -> dict:
    """
    From the subarray definition,
    get the list of frequency ids for a particular station beam

    :param subarray_def: dict of subarray parameters incl station beams
    :param stn_bm_id: station-beam ID
    """
    stn_bm_list = subarray_def["vis"]["stn_beams"]
    for bm_descr in stn_bm_list:
        if bm_descr["stn_beam_id"] == stn_bm_id:
            return bm_descr
    return {}


def _ipstr_2_word(ip_str: str) -> int:
    """
    Create a word representing the value of an ipv4 dotted address string
    eg "10.0.0.1" -> 0x0a000001
    """
    address_bytearray = inet_aton(ip_str)
    address_word = int.from_bytes(
        address_bytearray, byteorder="big", signed=False
    )
    return address_word


def _macstr_2_words(ip_str: str) -> (int, int):
    """
    Create words representing MAC address string
    eg "01-02-03-04-05-06" -> 0x0000102, 0x03040506
    """
    parts = ip_str.split("-")
    word0 = 0x0
    word1 = 0x0
    for count, p in enumerate(parts):
        if count < 2:
            word0 = (word0 << 8) + int(p, 16)
        else:
            word1 = (word1 << 8) + int(p, 16)
    return word0, word1


class SpeadSdp(FpgaPeripheral):
    """
    SPEAD packets out to SDP peripheral,
    for the CorrFpga FpgaPersonality
    """

    INIT_TEMPLATE_ADDR = 7168  # word offset
    TRIGGER_INIT = 1  # register bitfield value
    TRIGGER_END = 2  # register bitfield value
    TRIGGER_TIMEOUT = 5  # number of retries

    MAX_SDP_SERVERS = 16

    def enable(self):
        """
        Activate Packetiser so it can send packets
        """
        self.enable_packetiser = 0
        self.enable_packetiser = 1

    def disable(self):
        """
        disable packetiser
        """
        self.enable_packetiser = 0

    def configure_and_send_init_packets(
        self,
        baselines,
        first_coarse,
        visibility_channel,
        i_samples,
        subarray_id,
        subarray_beam,
        scan_id,
        beam_id,
    ):
        """
        Configure the SDP SPEAD initialisation template and send packets.

        Note: assumes hardware sends faster than we make requests!
        Note also: The number of spead-init packets sent (should be
        one per visibility channel) is determined by firmware
        (reads the subarray-beam-table entry at the index contained
        in the "current_subarray_beam_target" register)

        :param baselines: Number of baselines
        :param first_coarse: First coarse channel
        :param visibility_channel: Visibility channel ID
        :param i_samples: Number of samples integrated
        :param subarray_id: Subarray ID
        :param subarray_beam: Index into subarray_beam table
        :param scan_id: Scan ID
        :param beam_id: Beam ID
        """
        first_visibility_hz = first_visibility_freq(
            first_coarse,
            FINE_CH_PER_STANDARD_VIS_CH,
        )
        offsets, template = get_sdp_init(
            n_baselines=baselines,
            heap_id=heap_counter(
                subarray=subarray_id,
                beam=beam_id,
                visibility_channel=visibility_channel,
                integration_id=0,
            ),
            values={
                "Epoch": 0,
                "Chann": visibility_channel,
                "Basel": baselines,
                "ScaID": scan_id,
                "SrcID": ord("L"),  # L for SKA-Low telescope
                "Hardw": self._hardware_id,
                "BeaID": beam_id,
                "Subar": subarray_id,
                "Integ": i_samples * SEC_PER_FILTERBANK_SAMPL,
                "Frequ": 390_625 / 72,  # Exact Hertz
                "Resol": 32,  # 32-bit float
                "FreHz": round(first_visibility_hz),
                "ZoomI": 0,  # not a zoom
                "Firmw": self._packed_fw_version,
                "VisFl": 0,  # no need for flags in init packets
            },
        )
        self._set_template_packet(template, offsets)
        # Configure FPGA Hz calculation registers.
        # _base is the exact (float) value of the first visibility
        # the rest are calculated by applying _increment
        self.init_hertz_base = first_visibility_hz
        self.init_hertz_increment = (
            FINE_CHANNEL_HZ * FINE_CH_PER_STANDARD_VIS_CH
        )
        # FIXME recover locally without using exceptions in low-level code
        try:
            # Note this call programs the 'subarray_beam_target' register
            # and new subarray-beam-table must have been set up prior
            self.send_initial_packets(subarray_beam)
        except RuntimeError as err:
            self._logger.error(err)

    def _set_template_packet(
        self, packet: bytes, offsets: Dict[str, int]
    ) -> None:
        """
        Set the SPEAD initialisation template packet in FPGA memory.

        :param packet: SPEAD initialisation packet for FPGA to use as a
          template for generating all SPEAD initialisation packets.
        :param offsets: Dictionary of byte offsets for fields in ``packet``, keyed by
          SPEAD field name. We need keys "heap_counter", "FreHz", and "Chann".
          - ``heap_counter``: Offset of Heap Counter low byte. 6B long, high byte is at
          (offset - 5).
          - ``FreHz``: Offset of Frequency (Hertz) low byte. 4B long, high byte is at
          (offset + 3).
          - ``Chann``: Offset of Visibility Channel low byte. 4B long, high byte is at
          (offset + 3).
        """
        self._logger.debug("SpeadSdp setting template packet")
        padded_size = int(np.ceil(len(packet) / WORD_SIZE) * WORD_SIZE)
        data = np.zeros(padded_size, dtype=np.uint8)
        data[: len(packet)] = np.frombuffer(packet, dtype=np.uint8)
        data_to_write = data.view(dtype=ArgsWordType).byteswap(inplace=True)
        self.data[
            self.INIT_TEMPLATE_ADDR : self.INIT_TEMPLATE_ADDR
            + data_to_write.size
        ] = data_to_write

        # set FPGA registers to suit template data
        self.spead_init_byte_count = len(packet)
        self.ptr_heap_counter_in_init = offsets["heap_counter"]
        self.ptr_hz_field_in_init = offsets["FreHz"]
        self.ptr_vis_chan_id_in_init = offsets["Chann"]

    @property
    def init_hertz_base(self) -> IclField[float]:
        """Get init. packets base frequency."""
        integer = self.spead_init_hz_base_int.value
        fraction = self.spead_init_hz_base_frac.value
        return IclField(
            description="Initialisation packets - base frequency (Hz)",
            value=(float_from_binary_fraction(integer, fraction)),
        )

    @init_hertz_base.setter
    def init_hertz_base(self, value: float) -> None:
        """Set init. packets base frequency."""
        integer, fraction = binary_fraction(value)
        self.spead_init_hz_base_int = integer
        self.spead_init_hz_base_frac = fraction

    @property
    def init_hertz_increment(self) -> IclField[float]:
        """Get init. packets frequency increment per beam."""
        integer = self.spead_init_hz_incr_int.value
        fraction = self.spead_init_hz_incr_frac.value
        return IclField(
            description="Initialisation packets - increment per beam (Hz)",
            value=(float_from_binary_fraction(integer, fraction)),
        )

    @init_hertz_increment.setter
    def init_hertz_increment(self, value: float) -> None:
        """Set init. packets frequency increment per beam."""
        integer, fraction = binary_fraction(value)
        self.spead_init_hz_incr_int = integer
        self.spead_init_hz_incr_frac = fraction

    def _trigger_command(self, command_value: int):
        """
        Trigger SPEAD-END or SPEAD-INIT packets and wait for completion

        :param command_value: Either TRIGGER_INIT or TRIGGER_END
        """
        self.trigger_packet = 0  # reset just in case prior command failed
        self.trigger_packet = (
            command_value  # Write to FPGA and trigger packets
        )
        # Wait for command to complete
        for _ in range(self.TRIGGER_TIMEOUT):
            if self.spead_init_end_status == command_value:
                break
            time.sleep(0.01)
        else:
            # we didn't break out of for loop
            self._logger.error("SPEAD trigger didn't complete in time")
        self.trigger_packet = 0  # reset the trigger

    def send_initial_packets(self, subarray_beam: int):
        """
        Send SPEAD init packets using template in
        spead_params.data[7168:8161]
        Number-of-vis-chan pkts to send is obtained in firmware from the
        subarray-beam-table [inactive buffer] indexed by register
        "current_subarray_beam_target" programmed here

        :param subarray_beam: Index into subarray_beam table
        """
        self.current_subarray_beam_target = subarray_beam  # NB: register write
        self._trigger_command(self.TRIGGER_INIT)

    def send_end_packets(self):
        """
        Initiate firmware sending of spead-end-of-stream packets

        Packetiser sends end packets for entries in the subarray-beams table
        that are listed in the "list_subarrays_ending" RAM block
        A spead-end packet is sent for every fine channel of every
        subarray-beam table entry listed in the RAM block
        """
        self._trigger_command(self.TRIGGER_END)

    def set_subs_ending(
        self, buf_no, fin_list: list[int], mxc_no: int
    ) -> None:
        """
        Write table controlling spead-end packets to be sent on buffer flip

        :param fin_list: indicies of subarray-beam table entries
        that need spead-end packets sent
        """
        words = np.zeros(128, dtype=np.uint32)  # always write full table
        for cnt, val in enumerate(fin_list):
            words[cnt] = (val & 0x7F) | 0x80  # entry marked as valid
            txt = f"buf {buf_no} mxc_{mxc_no}.list_subarrays_ending[{cnt}]"
            txt += f" := {hex(words[cnt])}"
            self._logger.info(txt)
        if len(fin_list) == 0:
            txt = f"buf {buf_no} mxc_{mxc_no}.list_subarrays_ending[:] := zero"
            self._logger.info(txt)
        if buf_no == 0:
            self.list_subarrays_ending[0:128] = words  # write to FPGA
        else:
            self.list_subarrays_ending[128:256] = words  # write to FPGA

    def set_destination_table(self, buf_num, sbt_info, subarrays_descr):
        """
        Calculate entries for the 8k-word packetiser table that contains
        IP/UDP destinations, heap counter, heap size, n_ports, freq_chans
        corresponding to each entry in the subarray_beam table

        :param sbt_info: list of subarray_beam table entries, in order
        :param subarrays_descr: dict of subarray parameters (from
               internal_subarray attribute)
        :param buf_num: which buffer to write
        :return: 6.375kWord numpy array (to be written to packetiser RAM)
        """

        MAX_SERVERS = self.MAX_SDP_SERVERS
        ram = np.zeros(6528, dtype=np.uint32)
        for idx, entry in enumerate(sbt_info):
            (
                sa_id,
                stn_bm,
                first_coarse,
                first_fine,
                num_fine,
                i_time,
                i_chans,
                baselines,
            ) = entry  # this tuple format defined in register_tables.py
            # The subarray parameters in subarrays_descr are for all subarrays
            # and the entire subarray, not just the (small) freq slice we may
            # be processing

            n_vis_chans = num_fine // i_chans

            vis_out = _get_vis_output(subarrays_descr[str(sa_id)], stn_bm)
            sdp_hosts = vis_out["host"]  # eg: [[0, '192.168.1.10'], ]
            sdp_ports = vis_out["port"]  # eg: [[0, 9000, 1], ]
            if "mac" in vis_out:
                sdp_mac = vis_out["mac"]  # eg: [[0, '02-03-04-05-06-07'], ]
                # for now just use first MAC value in list
                (
                    self.ethernet_dst_mac_u,
                    self.ethernet_dst_mac_l,
                ) = _macstr_2_words(sdp_mac[0][1])

            # number of UDP ports each SDP host receives on
            n_host_ports = 65535 - sdp_ports[0][1]
            if len(sdp_ports) > 1:
                n_host_ports = sdp_ports[1][1] - sdp_ports[0][1]
            # UDP port base guaranteed same for all hosts, pick any
            host_udp_base_port = sdp_ports[0][1]
            freq_no = get_stn_bm_frq_no(
                subarrays_descr[str(sa_id)], stn_bm, first_coarse
            )

            # which server is first one this particular alveo sends to?
            server_no = (freq_no * 144) // n_host_ports
            first_port = host_udp_base_port + ((freq_no * 144) % n_host_ports)
            num_servers = math.ceil(n_vis_chans / n_host_ports)

            ram[0x0000 + MAX_SERVERS * idx] = _ipstr_2_word(
                sdp_hosts[server_no][1]
            )

            # logging for debug only
            ip_word = _ipstr_2_word(sdp_hosts[server_no][1])
            self._logger.info(
                "server_no=%d, num_servers=%d", server_no, num_servers
            )
            self._logger.info("first_port=%d", first_port)
            self._logger.info("sbm index=%d <- IP=0x%x", idx, ip_word)
            self._logger.info("baselines=%d", baselines)
            self._logger.info("first_fine=%d", first_fine)
            self._logger.info("num_fine=%d", num_fine)
            self._logger.info("i_chans=%d", i_chans)

            ram[0x0800 + MAX_SERVERS * idx] = first_port
            for server in range(1, num_servers):
                ram[0x0000 + MAX_SERVERS * idx + server] = _ipstr_2_word(
                    sdp_hosts[server_no + server][1]
                )
                ram[0x0800 + MAX_SERVERS * idx + server] = host_udp_base_port

            # Heap counter is a combination of: subarray, station beam, visibility
            # channel/frequency ID, and integration ID. The FPGA will append the
            # integration ID to the value we configure here.
            ram[0x1000 + MAX_SERVERS * idx] = heap_counter(
                sa_id, stn_bm, freq_no * 144
            )
            # Heap size, 34 bytes per baseline + 8 bytes TCI
            ram[0x1800 + idx] = baselines * 34 + 8
            # Block size step per sub array (== No of ports per SDP host)
            ram[0x1880 + idx] = n_host_ports
            # no of vis frequency channels in subarray
            ram[0x1900 + idx] = n_vis_chans

        if buf_num == 0:
            self.data[0:6528] = ram
        else:
            self.data[8192 : 8192 + 6528] = ram

    @property
    def _packed_fw_version(self) -> int:
        """
        FPGA Firmware version packed into 4 bytes.

        major.minor.patch.prerelease (major being the high byte)

        For releases, the prerelease field will be zero.
        For non-release builds, the prerelease field will contain the lower case form of
        the first non-whitespace character of the build type (e.g. "d" for "dev").

        e.g. For Firmware version 1.2.3+dev<commit>, we get 0x01020364
        """
        prerelease = 0
        sys_peripheral = self._personality.system
        if hasattr(sys_peripheral, "build_type"):
            # build_type register was added to firmware in Sep 2023
            build_type = (
                int.to_bytes(sys_peripheral.build_type.value, WORD_SIZE, "big")
                .decode(encoding="ascii")
                .lower()
                .strip()
            )
            if build_type != "rel":
                # ASCII marker for prerelease, first character of build type
                prerelease = ord(build_type[0])
                if prerelease == 0:
                    # in case we get zero in error
                    prerelease = ord("!")
        return (
            ((sys_peripheral.firmware_major_version & 0xFF) << 24)
            | ((sys_peripheral.firmware_minor_version & 0xFF) << 16)
            | ((sys_peripheral.firmware_patch_version & 0xFF) << 8)
            | (prerelease & 0xFF)
        )

    @property
    def _hardware_id(self) -> int:
        """
        The hardware ID is 32-bits and needs to be unique across all the
        hardware that produces visibilities.
        """
        # use the low 4 bytes of the FPGA's first MAC address
        info = self._personality.info
        if info:
            return int(
                "".join(info["platform"]["macs"][0]["address"].split(":")[2:]),
                16,
            )
        return 0xABCD0123
