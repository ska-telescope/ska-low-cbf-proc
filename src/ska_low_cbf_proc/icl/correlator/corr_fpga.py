# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.
"""
Interface to Correlator FPGA Image

Defines the "peripherals" in the FPGA that the control system will monitor
Defines which aspects of the peripherals to ignore (nodata, noattributes)
"""
import os
import time

import numpy as np
from ska_low_cbf_fpga import FpgaPersonality
from ska_low_cbf_fpga.args_fpga import WORD_SIZE

# from ska_low_cbf_proc.icl.correlator.corr_expander import (
#     get_num_subarray_beams,
#     get_total_channels,
#     get_words_demap,
#     get_words_subarray_beams,
#     get_words_vct,
# )
from ska_low_cbf_proc.icl.correlator.spead_sdp import (
    SpeadSdp,
    get_stn_bm_frq_no,
)
from ska_low_cbf_proc.icl.no_attributes import NoAttributes
from ska_low_cbf_proc.icl.no_data import NoData

NUM_BUFFERS = 2  # double-buffered
# These register lengths are for one of the two buffers
VCT_LEN_BYTES = 2048 * WORD_SIZE  # 1024 entries * 2word/entry
DEMAP_LEN_BYTES = 512 * WORD_SIZE  # 256 entries * 2word/entry
SUB_BM_LEN_BYTES = 1024 * WORD_SIZE  # 256 entries * 4w/entry
SUB_BM_TBL_BYTES = 1 * WORD_SIZE  # 1 entries 1w/entry
DEL_TBL_LEN_BYTES = 4096 * WORD_SIZE  # 1k entry * 4 w/entry
CT1_START_PKT_BYTES = 1 * WORD_SIZE  # 1 entry * 1word

PKTS_IN_CT1_BUF = 408  # 408 -> 902.48msec of data
SUBARRAY_BEAM_TBL_LEN_PER_MXC = 128

N_VCHAN = 1024
INGEST_STATS_WORDS_PER_VCHAN = 8
INGEST_STATS_PKT_CNT_LO_OFFSET = 4
INGEST_STATS_PKT_CNT_HI_OFFSET = 5


class CorrFpga(FpgaPersonality):
    """
    Class providing access to Correlator FPGA by translating
    high-level operations (eg VCT update) into lower-level register
    reads and writes

    The implementation of the class methods depends intimately on the
    register names in the FPGA VHDL, plus the function and interaction of
    the registers in the VHDL design. If the VHDL design changes, this code
    may also need to change
    """

    _peripheral_class = {
        "drp": NoAttributes,
        # "system": NoAttributes, # fixme implement?
        "vitis_shared": NoData,
        "cmac_b": NoAttributes,
        "config": NoAttributes,  # fixme implement
        "corr_ct1": NoAttributes,  # fixme implement
        "corr_ct2": NoAttributes,  # fixme implement
        "lfaadecode100g": NoData,
        "spead_sdp": SpeadSdp,
        "spead_sdp_2": SpeadSdp,
        "timeslave": NoAttributes,  # fixme implement
    }

    _not_user_methods = set()
    _not_user_attributes = set()

    VCT_BASE = 8192
    """VCT offset within LFAA data RAM"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._check_fw("CORR", ">=0.1.1")
        # Firmware buffer flips introduced in 0.1.1
        assert (
            self.system.args_map_build.value >= 0x25021320
        ), "Use newer firmware!"

        # Save addresses we'll access when updating FPGA register values
        # Note: Use lower-case register names regardless of VHDL case
        periph = self["system"]
        self._uptime = periph["time_uptime"].address
        self._build = periph["build_date"].address
        self._n_corr = periph["no_of_correlator_instances"].value

        periph = self["lfaadecode100g"]
        self._lfaa_reset = periph["lfaa_decode_reset"].address  # 1 word
        self._vct = None
        """Virtual Channel Table base address. 2 words * 1024 entries * 2 buffers"""
        self._vct = periph["data"].address + self.VCT_BASE * WORD_SIZE
        # self._vct_table_sel = periph["table_select"].address  # 1 word
        # self._vct_tot_ch = periph["total_channels"].address  # 1 word
        tch_0 = periph["total_channels_table0"].address
        tch_1 = periph["total_channels_table1"].address
        self._vct_tot_ch = [tch_0, tch_1]
        self._spead_counts = periph["spead_packet_count"].address  # 5 words
        self._vcstats = periph["data"].address  # 8 words * 1024 vchans
        # self._hbm_reset = periph["hbm_reset"].address
        # self._hbm_status = periph["hbm_reset_status"].address

        periph = self["corr_ct1"]
        self._ct1_table_sel = periph["table_select"].address
        self._ct1_table_used = periph["table_in_use"].address
        self._ct1_stats = periph["early_or_late_count"].address  # 6 words
        self._ct1_vc_pkts_out = periph["correlator_output_count"].address
        self._ct1_hbm_frame_addr = periph["integration_count"].address
        self._ct1_input_pkts = periph["input_packets"].address

        periph = self["corr_ct2"]
        # self._ct2_tbl_sel = periph["table_select"].address  #
        # n_sub_bms has one entry for each MxC and is double buffered
        self._ct2_stats = periph["bufferoverflowerror"].address  # 12 Words
        self._ct2_vc_dmap = periph["vc_demap"].address  # 2w * 256 *2buf
        self._ct2_sub_bm = periph["subarray_beam"].address  # 4w*128*2MxC*2buf
        b0mxc0 = periph["buf0_subarray_beams_table0"].address
        b1mxc0 = periph["buf0_subarray_beams_table1"].address
        b0mxc1 = periph["buf1_subarray_beams_table0"].address
        b1mxc1 = periph["buf1_subarray_beams_table1"].address
        self._ct2_n_sub_bm = [[b0mxc0, b0mxc1], [b1mxc0, b1mxc1]]
        # Zero in case k8s gave us a pre-used FPGA
        for buf in range(0, 2):
            for mxc in range(0, 2):
                self.driver.write(self._ct2_n_sub_bm[buf][mxc], 0x0)

        self._buffer_in_use = 0  # 0 so first buffer used will be 1
        self.tot_ch = 0  # latest value in total_channels register

        # Although subarray beam table alway has entries for two
        # matrix correlators (0-127, 128-256), sometimes the second packetiser
        # and second matrix correlator won't be present in the FPGA
        # because the FPGA designer has removed it for a faster build
        self._spead_packetiser = [self.spead_sdp]
        self.spead_sdp.disable()  # (in case k8s gave us a pre-used FPGA)
        if self._n_corr == 2:
            self._spead_packetiser.append(self.spead_sdp_2)
            self.spead_sdp_2.disable()  # (in case k8s gave us a pre-used FPGA)
        else:
            self._spead_packetiser.append(None)  # no second packetiser
        self._pktzrs_enabld = [False] * 2

        # Make sure ingest is in reset (in case k8s gave us a pre-used FPGA)
        self.driver.write(self._lfaa_reset, 1)

        # TODO still needed in latest FPGA??
        # CT2 needs a reset in current FPGA versions (waiting on firmware fix)
        #  This removes need for DebugRegWrite of "corr_ct1.full_reset" that
        #  was prevously used to reset CT2 (via CT1 register)
        self["corr_ct1"]["full_reset"] = 1
        self["corr_ct1"]["full_reset"] = 0

        self._hbm_pf = [None, None]
        if "HBM_DBG" in os.environ:
            periph = self["hbm_rd_debug"]
            self._hbm_pf[0] = periph["debug_pageflip"].address
            periph = self["hbm_rd_debug_2"]
            self._hbm_pf[1] = periph["debug_pageflip"].address

    def wait_for_flip_get_next_buf(self) -> int:
        """Wait for previous buffer flip complete and get next buffer to use"""
        # TODO this doesn't yet consider packetiser
        tab_sel = self.driver.read(self._ct1_table_sel, 1)
        tab = (tab_sel >> 1) & 0x1
        expected_used = 0x0 if tab == 0 else 0x3
        tab_used = self.driver.read(self._ct1_table_used, 1)
        t_start = time.time()
        deadline = t_start + 2.0
        while time.time() < deadline:
            tab_used = self.driver.read(self._ct1_table_used, 1)
            if (tab_used & 0x3) == expected_used:
                txt = ""
                break
            time.sleep(0.01)  # expected slow operation
        else:
            txt = "Timeout!"
        t_end = time.time()
        txt += f"{t_end-t_start:.2f} s waiting for previous flip to complete"
        txt += f" [tab_select=0x{tab_sel:x}, tab_in_use=0x{tab_used:x}]"
        spd_pkts = self.driver.read(self._spead_counts, 1)
        ct1_pkts = self.driver.read(self._ct1_input_pkts, 1)
        txt += f", spead_pkts={spd_pkts}, ct1_input_pkts={ct1_pkts}"
        if t_end >= deadline:
            self._logger.error(txt)
        else:
            self._logger.info(txt)

        next_buf = 1 if tab == 0 else 0
        return next_buf

    def wait_for_data(self) -> bool:
        """Wait for SPS data to start flowing"""
        initial_pkts = self.driver.read(self._ct1_input_pkts, 1)
        t_start = time.time()
        deadline = t_start + 2 * 0.85  # >two frame times
        while time.time() < deadline:
            time.sleep(0.01)
            pkts = self.driver.read(self._ct1_input_pkts, 1)
            if pkts != initial_pkts:  # got at least one packet?
                return True
        txt = f"No SPS data: CT1 packets{pkts}"
        self._logger.warning(txt)
        return False

    def log_flip(self, log_time: float):
        for _ in range(int(log_time / 0.02)):
            pkt_tsel = self._spead_packetiser[0]["table_sel_status"].value
            ct1_tab_used = self.driver.read(self._ct1_table_used, 1)
            ct1_tab_sel = self.driver.read(self._ct1_table_sel, 1)
            txt = f"corr_ct1.table_select: {hex(ct1_tab_sel)}"
            txt += f"  corr_ct1.table_in_use: {hex(ct1_tab_used)}"
            txt += f"  spead_sdp.table_sel_status: {hex(pkt_tsel)}"
            self._logger.info(txt)
            time.sleep(0.05)
        return

    def wait_for_packetiser_flip_complete(self):
        """
        Wait/block until previous buffer flip completed by packetiser.

        This may only approximate
        """
        t_start = time.time()
        deadline = t_start + 2.0
        while time.time() < deadline:
            flipped = []
            for cnt, pktzr in enumerate(self._spead_packetiser):
                if pktzr is None or (not self._pktzrs_enabld[cnt]):
                    continue
                tsel = pktzr["table_sel_status"].value
                same_tables_selected = ((tsel & 0x20) >> 5) == (tsel & 0x01)
                no_flips_in_progress = (tsel & 0x02) == 0
                flipped.append(same_tables_selected and no_flips_in_progress)

            if all(flipped):
                break
            time.sleep(0.01)
        else:
            self._logger.error("Packetiser flip did not clear")
        t_elapsed = time.time() - t_start
        txt = f"{t_elapsed:.2f} s waiting for packetiser flip to complete"
        txt += ", table_sel_status = "
        for pktzr in self._spead_packetiser:
            if pktzr is None:
                continue
            txt += f' {hex(pktzr["table_sel_status"].value)}'
        # TODO removeme (debug)
        if self._hbm_pf[0] is not None:
            dbg_pf0 = self._driver.read(self._hbm_pf[0], 1)
            dbg_pf1 = self._driver.read(self._hbm_pf[1], 1)
            txt += f", hbm_dbg_pageflip = {hex(dbg_pf0)} {hex(dbg_pf1)}"
        spd_pkts = self.driver.read(self._spead_counts, 1)
        ct1_pkts = self.driver.read(self._ct1_input_pkts, 1)
        txt += f", spead_pkts={spd_pkts}, ct1_input_pkts={ct1_pkts}"
        self._logger.info(txt)
        return

    def clear_vcts(self):
        """Make all VCT tables completely invalid and unused"""
        for buf in range(0, NUM_BUFFERS):
            self.prgm_vct(
                buf, np.zeros(VCT_LEN_BYTES // WORD_SIZE, dtype=np.uint32), 0
            )

    def prgm_vct(self, buf, words, n_ch) -> None:
        """Write VCT info to buffer"""
        np_words = np.asarray(words, dtype=np.uint32)
        self.driver.write(self._vct + buf * VCT_LEN_BYTES, np_words)
        self._driver.write(self._vct_tot_ch[buf], n_ch)

    def prgm_demap(self, buf, words) -> None:
        """Write Demap Table into buffer"""
        np_words = np.asarray(words, dtype=np.uint32)
        self.driver.write(self._ct2_vc_dmap + buf * DEMAP_LEN_BYTES, np_words)

    def prgm_sbt(self, buf, words) -> None:
        """Write subarray-beam table into buffer"""
        np_words = np.asarray(words, dtype=np.uint32)
        self.driver.write(self._ct2_sub_bm + buf * SUB_BM_LEN_BYTES, np_words)

    def prgm_num_sbt_entries(self, buf_num, ent):
        """
        Write number of sbt entries in MxC_0 and MxC_1 for new buffer
        """
        for mxc_cnt, val in enumerate(ent):
            self.driver.write(self._ct2_n_sub_bm[buf_num][mxc_cnt], val)

    def get_num_mxc(self) -> int:
        """How many matrix correlators implemented in this FPGA"""
        return self._n_corr

    def prgm_pktzr(self, buf_num, sbt_info, intrnl_subarray):
        """Program the packetiser"""
        for mxc_num, pktzr in enumerate(self._spead_packetiser):
            if pktzr is not None:
                pktzr.set_destination_table(
                    buf_num, sbt_info[mxc_num], intrnl_subarray
                )

    def pktzr_enable(self, sbt_info):
        """Enable or disable packetisers if used or not_used respectively."""
        for cnt, pktzr in enumerate(self._spead_packetiser):
            if pktzr is None:
                continue  # skip if packetiser not implemented in FPGA
            if (len(sbt_info[cnt]) != 0) and (not self._pktzrs_enabld[cnt]):
                pktzr.enable()
                self._pktzrs_enabld[cnt] = True
                txt = f"Enable pktzr {cnt}"
            elif (len(sbt_info[cnt]) == 0) and self._pktzrs_enabld[cnt]:
                pktzr.disable()
                self._pktzrs_enabld[cnt] = False
                txt = f"Disable pktzr {cnt}"
            else:
                txt = (
                    f"No change enable_pktzr{cnt} = {self._pktzrs_enabld[cnt]}"
                )
            self._logger.info(txt)

    def prgm_pktzr_spead_end(self, buf_no, saved_sbt_info, ending_ids):
        """Set up table for spead end packet generation"""
        for mxc_no, pktzr in enumerate(self._spead_packetiser):
            if pktzr is not None:
                finishing = []
                for cnt, itm in enumerate(saved_sbt_info[mxc_no]):
                    subarray_id = itm[0]
                    if subarray_id in ending_ids:
                        finishing.append(cnt)
                self._spead_packetiser[mxc_no].set_subs_ending(
                    buf_no, finishing, mxc_no
                )

    def flip_to_remove(self, buf) -> None:
        """Trigger buffer flip to remove subarrays"""
        regval = buf << 1
        self.driver.write(self._ct1_table_sel, regval)
        txt = (
            f"Flip buffers: SubarrayRemove: new_buf={buf} table_sel:={regval}"
        )
        self._logger.info(txt)

    def flip_to_add(self, buf) -> None:
        """Trigger buffer flip to add subarrays"""
        regval = (buf << 1) | 0x1
        self.driver.write(self._ct1_table_sel, regval)
        txt = f"Flip buffers: SubarrayAdd: new_buf={buf} table_sel:={regval}"
        self._logger.info(txt)

    def send_ends_and_stop(self) -> None:
        """Stop FPGA processing of data"""
        _ = self.wait_for_flip_get_next_buf()  # wait for any in-progress flips
        self.wait_for_packetiser_flip_complete()
        # send spead-end packets manually when completely stopped
        self._logger.info("FPGA shutdown - Manually send spead-end")
        self._send_manual_spead_end()
        # turn off packetisers, by indicating nothing assigned to each mxc
        self._logger.info("FPGA shutdown - disable packetisers, reset ingest")
        self.pktzr_enable([[], []])
        self.driver.write(self._lfaa_reset, 1)

    def prep_for_start(self) -> None:
        """
        Prepares FPGA when starting from no active subarrays running.
        """
        # Zero so initial invalid poly will result in zero delay applied
        # FPGA will use buffer 0 if both are invalid. Set buffer in use
        # to 0 so first poly goes to buffer 1 & zero remains in buf 0
        # Then if first poly isn't valid (yet) the zero will be used
        self._buffer_in_use = 0  # buffer for delay poly
        self.zero_all_delay_polys()
        self.driver.write(self._lfaa_reset, 1)  # ensure reset is asserted

    def start(self) -> None:
        """
        Actually begin FPGA processing.

        Called after VCT & Demap tables have valid entries
        """
        self.driver.write(self._lfaa_reset, 0)  # release reset

    def _send_manual_spead_end(self):
        """
        Trigger hardware to send spead-end packets, and wait until all sent
        """
        #  Send spead-end packets for all ending subarrays
        for mxc_num in range(0, self.get_num_mxc()):
            # First list entry non-zero means there are spead-ends to send
            lse0 = self._spead_packetiser[mxc_num].list_subarrays_ending[0]
            if lse0 != 0:
                txt = f"Sending manual SPEAD-ENDs for MxC_{mxc_num}"
                self._logger.info(txt)
                # Trigger firmware to send ends for all entries in RAM list
                # note: this call waits until all packets are sent (~usec)
                self._spead_packetiser[mxc_num].send_end_packets()

    def send_spead_start(self, starting_sa, spead_info, intrnl_subarray):
        """
        Send spead start packets for subarrays that are starting.

        Start packets must be sent from the correct packetiser (the one that is
        handling the correlation data output)
        """

        n_start_pkts = 0  # counter of spead-start packets we send
        n_starts = [{} for _ in range(0, self.get_num_mxc())]
        for mxc_num, mxc_regs in enumerate(spead_info):
            for sb_index, reg_val in enumerate(mxc_regs):
                (
                    sa_id,
                    stn_bm,
                    first_coarse,
                    first_fine,  # Not used?!
                    num_fine,  # total number of fine channels
                    i_samples,
                    i_chans,  # number of fine channels integrated per visibility
                    baselines,
                ) = reg_val

                # only send if this subarray is starting
                if sa_id not in starting_sa:
                    continue

                n_vis_chans = num_fine // i_chans
                scan_id = intrnl_subarray[str(sa_id)]["scan_id"]

                sdp_packetiser: SpeadSdp = self._spead_packetiser[mxc_num]
                if sdp_packetiser is None:
                    continue  # should this raise an error?
                visibility_id = (
                    get_stn_bm_frq_no(
                        intrnl_subarray[str(sa_id)], stn_bm, first_coarse
                    )
                    * 144
                )
                sdp_packetiser.configure_and_send_init_packets(
                    baselines,
                    first_coarse,
                    visibility_id,
                    i_samples,
                    sa_id,
                    sb_index,
                    scan_id,
                    stn_bm,
                )

                n_start_pkts += n_vis_chans
                if sa_id not in n_starts[mxc_num]:
                    n_starts[mxc_num][sa_id] = 0
                n_starts[mxc_num][sa_id] += n_vis_chans
        if n_start_pkts != 0:
            for mxc_num, data in enumerate(n_starts):
                for sa_id, npkt in data.items():
                    txt = f"SPEAD INIT [mxc_{mxc_num}] subarray_{sa_id}: "
                    txt += f"{npkt} packets"
                    self._logger.info(txt)
            self._logger.info(
                "SPEAD INIT - total %d start packets", n_start_pkts
            )

    def read_packet_counts(self):
        """
        Read most recent packet count received for each VirtualChannel

        :return: list of counters, one per virtual chan
        """
        n_words = N_VCHAN * INGEST_STATS_WORDS_PER_VCHAN
        pkt_cnts = self._driver.read(self._vcstats, n_words)
        pkt_cnt_low = pkt_cnts[
            INGEST_STATS_PKT_CNT_LO_OFFSET:n_words:INGEST_STATS_WORDS_PER_VCHAN
        ]
        pkt_cnt_hi = pkt_cnts[
            INGEST_STATS_PKT_CNT_HI_OFFSET:n_words:INGEST_STATS_WORDS_PER_VCHAN
        ]
        full_pkt_cnt = pkt_cnt_hi * (2**32) + pkt_cnt_low
        return full_pkt_cnt.tolist()

    def zero_packet_counts(self):
        """Clear packet counters in FPGA registers."""
        n_words = N_VCHAN * INGEST_STATS_WORDS_PER_VCHAN
        np_data = np.zeros(n_words, dtype=np.uint32)
        self.driver.write(self._vcstats, np_data)

    def get_ct1_frame_count(self) -> int:
        """Read current frame counter from Corner Turn #1"""
        frame_count = self._driver.read(self._ct1_hbm_frame_addr, 1)
        return frame_count

    def fpga_status(self) -> dict:
        """
        Gather status indication for FPGA processing operations
        """
        summary = {}
        summary["fpga_uptime"] = self["system"]["time_uptime"].value
        summary["total_pkts_in"] = self["system"][
            "eth100g_rx_total_packets"
        ].value
        summary["spead_pkts_in"] = self["lfaadecode100g"][
            "spead_packet_count"
        ].value
        summary["total_pkts_out"] = self["system"][
            "eth100g_tx_total_packets"
        ].value
        summary["vis_pkts_out"] = self["spead_sdp"]["no_of_packets_sent"].value
        if self._n_corr == 2:
            summary["vis_pkts_out"] += self["spead_sdp_2"][
                "no_of_packets_sent"
            ].value
        return summary

    def update_delay_polys(self, buffer_pair: list) -> None:
        """
        Write delay polynomial data into firmware registers

        :param buffer_pair: Two-element list, each element a 20kword list
        of integers to be written to FPGA delay polynomial buffers as uint32
        """
        # The delay registers may not yet exist in firmware
        try:
            # Have to convert to numpy arrays for FPGA driver (FIXME?)
            self["corr_ct1"]["data"][0:20480] = np.asarray(
                buffer_pair[0], dtype=np.uint32
            )
            self["corr_ct1"]["data"][20480:40960] = np.asarray(
                buffer_pair[1], dtype=np.uint32
            )
        except (KeyError, ValueError):
            self._logger.warning("Can't write delays into register")
        return

    def zero_all_delay_polys(self):
        """Zero every stn-beam delay value in FPGA registers"""
        self.update_delay_polys(
            (
                np.zeros(20 * 1024, dtype=int),
                np.zeros(20 * 1024, dtype=int),
            )
        )
