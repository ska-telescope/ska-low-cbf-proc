# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE for more info.
"""
stand-alone code for generation of init packets
"""
import string
import struct
from typing import Dict

# HEX codes for SPEAD item identifiers
SPEAD_HEAP_CTR = b"\x80\x01"
SPEAD_HEAP_SIZ = b"\x80\x02"
SPEAD_HEAP_OFS = b"\x80\x03"
SPEAD_PAY_LEN = b"\x80\x04"
SPEAD_ITM_DESC = b"\x00\x05"
SPEAD_DESC_NAME = b"\x00\x10"
SPEAD_DESC_HUMAN = b"\x00\x11"
SPEAD_DESC_SHAPE = b"\x00\x12"
SPEAD_DESC_TYPE = b"\x00\x13"
SPEAD_DESC_ID = b"\x80\x14"
SPEAD_DESC_NUMPY = b"\x00\x15"
# Standard header for SPEAD packets
SPEAD_PKT_HEADER = b"\x53\x04\x02\x06\x00\x00"
SPEAD_STREAM_START = b"\x80\x06\x00\x00\x00\x00\x00\x00"


def spead_item_bytes(item_id: bytes, value: int) -> bytes:
    """
    :param item_id: 2-byte SPEAD hex-code for item
    :param value: integer value for item
    :return: 8-byte representation of SPEAD item
    """
    return item_id + int.to_bytes(value, 6, byteorder="big")


def get_item_descriptor_payload(
    heap_ct, human_descr, var_name, shape, descr_id
) -> bytes:
    """
    Create the SPEAD "heap" to initialise a new variable to be transferred.
    """
    payload_bytes = b""
    n_itms = 0

    # variable-length items first (to determine space used)
    hd2_bytes = b""
    # 1. descriptor - name of variable
    offset = len(payload_bytes)
    payload_bytes += bytes(var_name, "utf8")
    hd2_bytes += spead_item_bytes(SPEAD_DESC_NAME, offset)
    n_itms += 1
    # 2. descriptor - optional description, human readable
    if human_descr != "":
        offset = len(payload_bytes)
        payload_bytes += bytes(human_descr, "utf8")
        hd2_bytes += spead_item_bytes(SPEAD_DESC_HUMAN, offset)
        n_itms += 1
    # 3. descriptor - shape: numpy dtype string
    offset = len(payload_bytes)
    payload_bytes += bytes(shape, "utf8")
    hd2_bytes += spead_item_bytes(SPEAD_DESC_SHAPE, offset)
    n_itms += 1
    # 4. descriptor - type: same numpy dtype string
    hd2_bytes += spead_item_bytes(SPEAD_DESC_TYPE, offset)
    n_itms += 1
    # 5. descriptor - dtype: same numpy dtype string
    hd2_bytes += spead_item_bytes(SPEAD_DESC_NUMPY, offset)
    n_itms += 1

    payload_len = len(payload_bytes)

    # fixed-length items in hd1
    hd1_bytes = b""
    # 1. Heap counter
    hd1_bytes += spead_item_bytes(SPEAD_HEAP_CTR, heap_ct)
    n_itms += 1
    # 2. Heap size (only the payload in this heap)
    hd1_bytes += spead_item_bytes(SPEAD_HEAP_SIZ, payload_len)
    n_itms += 1
    # 3. Heap offset (always start offset zero)
    hd1_bytes += spead_item_bytes(SPEAD_HEAP_OFS, 0)
    n_itms += 1
    # 4. Packet payload length
    hd1_bytes += spead_item_bytes(SPEAD_PAY_LEN, payload_len)
    n_itms += 1
    # 5. descriptor - ID
    hd1_bytes += spead_item_bytes(SPEAD_DESC_ID, descr_id)
    n_itms += 1

    # spead header for this section
    spead_bytes = SPEAD_PKT_HEADER + int.to_bytes(n_itms, 2, byteorder="big")

    return spead_bytes + hd1_bytes + hd2_bytes + payload_bytes


def get_item_descriptor_header(itm_start: int) -> bytes:
    """
    A SPEAD Item Descriptor is an 8-byte record in the SPEAD header that
    points to where the Descriptor content is located in the payload
    """
    desc_bytes = SPEAD_ITM_DESC
    desc_bytes += int.to_bytes(itm_start, 6, byteorder="big")
    return desc_bytes


def send_value(
    var_id: int, npy_fmt: dict, value, hdr: bytes, payload: bytes
) -> (bytes, bytes, int | None):
    """
    Send a value for a variable.

    If the variable will fit in <= 6 bytes then it can be sent as immediate value,
    otherwise it requires space in the SPEAD payload section to accommodate lengthier
    data.

    :returns: (header bytes, payload bytes, offset)
    Where offset is the end of the immediate value, or None for indirect values.
    """
    # assume it is immediate (since most are small enough to be)
    hdr_bytes = struct.pack(">H", 0x8000 + var_id)

    fmt_str = npy_fmt["descr"]
    immediate_value_offset = None
    if fmt_str == "<u1":
        hdr_bytes += b"\x00\x00\x00\x00\x00" + struct.pack("B", value)
        # calculating the offsets as start + len, in case we need to change back to
        # using the start address in the future
        immediate_value_offset = 7 + 1
    elif fmt_str == "<u2":
        hdr_bytes += b"\x00\x00\x00\x00" + struct.pack("<H", value)
        immediate_value_offset = 6 + 2
    elif fmt_str == "<u4":
        hdr_bytes += b"\x00\x00" + struct.pack("<I", value)
        immediate_value_offset = 4 + 4
    elif fmt_str == "<u8":
        # rewrite header since value too large to be immediate
        offset = len(payload)
        # header indicates where in payload the variable value is placed
        hdr_bytes = struct.pack(">H", var_id)
        hdr_bytes += struct.pack(">H", ((offset >> 8) & 0x0_FFFF))
        hdr_bytes += struct.pack(">I", (offset & 0x0_FFFF_FFFF))
        # payload contains the actual value
        payload += struct.pack("<Q", value)
    elif fmt_str == "<f4":
        hdr_bytes += b"\x00\x00" + struct.pack("<f", value)
        immediate_value_offset = 4 + 4
    elif fmt_str == "|S1":
        hdr_bytes += b"\x00\x00\x00\x00\x00" + struct.pack("B", value)
        immediate_value_offset = 7 + 1
    else:
        print(f"OOPS! Program missing format string '{fmt_str}'")

    if immediate_value_offset is not None:
        immediate_value_offset += len(hdr)

    hdr += hdr_bytes

    return hdr, payload, immediate_value_offset


def remove_whitespace(x: str) -> str:
    """Remove all whitespace from a string."""
    return x.translate(str.maketrans("", "", string.whitespace))


def get_sdp_init(
    n_baselines: int, heap_id: int, values: Dict[str, int]
) -> (Dict[str, int], bytes):
    """
    Get spead init packet template for SDP data.

    :param n_baselines: Number of baselines
    :param heap_id: Heap ID (also known as Heap Counter)
    :param values: key: SPEAD item short name, value: value
    :returns: an offsets dict that maps field name to a byte offset,
      the packet bytes
    """

    # numpy format descriptions, sent in SPEAD heaps that
    # initialise transfer of any new variable
    u1_type = {"descr": "<u1", "fortran_order": False, "shape": ()}
    u2_type = {"descr": "<u2", "fortran_order": False, "shape": ()}
    u4_type = {"descr": "<u4", "fortran_order": False, "shape": ()}
    u8_type = {"descr": "<u8", "fortran_order": False, "shape": ()}
    f4_type = {"descr": "<f4", "fortran_order": False, "shape": ()}
    s1_type = {"descr": "|S1", "fortran_order": False, "shape": ()}
    cor_type = {
        "descr": [("VIS", "<c8", (4,)), ("TCI", "|i1"), ("FD", "|u1")],
        "fortran_order": False,
        "shape": (n_baselines,),
    }

    sdp_items = (
        (0x6002, "Chann", u4_type, values["Chann"], True),
        (0x6013, "Epoch", u4_type, values["Epoch"], False),
        (0x6005, "Basel", u4_type, values["Basel"], False),
        (0x6008, "ScaID", u8_type, values["ScaID"], False),
        (0x6015, "SrcID", s1_type, values["SrcID"], False),
        (0x6009, "Hardw", u4_type, values["Hardw"], False),
        (0x600B, "BeaID", u2_type, values["BeaID"], False),
        (0x600C, "Subar", u2_type, values["Subar"], False),
        (0x600D, "Integ", f4_type, values["Integ"], False),
        (0x600E, "Frequ", f4_type, values["Frequ"], False),
        (0x6010, "FreHz", u4_type, values["FreHz"], True),
        (0x600F, "Resol", u1_type, values["Resol"], False),
        (0x6011, "ZoomI", u1_type, values["ZoomI"], False),
        (0x6012, "Firmw", u4_type, values["Firmw"], False),
        (0x6016, "VisFl", u4_type, values["VisFl"], False),
        (0x6014, "tOffs", u8_type, None, False),
        (0x600A, "Corre", cor_type, None, False),
    )
    # Fields are variable_id, variable_name, numpy_format string, value,
    #  record_offset.
    # Values for every variable except visibilities (0x600A, Corre)
    # are sent when the transfer is initiated

    payload_bytes = b""
    header_bytes = b""

    n_spead_items = 0
    value_offsets = {}
    # We (apparently) need to put our indirect-referenced values up front.
    for var_id, var_name, npy_descr, value, record_offset in sdp_items:
        # The only indirect value is 'ScaID', so let's just select that
        if value is None or var_name != "ScaID":
            continue
        header_bytes, payload_bytes, locn = send_value(
            var_id, npy_descr, value, header_bytes, payload_bytes
        )
        n_spead_items += 1
        if record_offset:
            value_offsets[var_name] = locn

    # SPEAD descriptors for each variable to be transferred
    for var_id, var_name, numpy_descr, _, _ in sdp_items:
        # SPEAD payload part
        pkt_bytes = get_item_descriptor_payload(
            heap_id, "", var_name, remove_whitespace(str(numpy_descr)), var_id
        )
        payload_offset = len(payload_bytes)
        payload_bytes += pkt_bytes
        n_spead_items += 1

        # SPEAD header part
        header_bytes += get_item_descriptor_header(payload_offset)

    # Values for each immediate variable that has a static value
    for var_id, var_name, npy_descr, value, record_offset in sdp_items:
        # We used indirect addressing for "ScaID" above
        if value is None or var_name == "ScaID":
            continue
        header_bytes, payload_bytes, locn = send_value(
            var_id, npy_descr, value, header_bytes, payload_bytes
        )
        n_spead_items += 1
        if record_offset:
            value_offsets[var_name] = locn

    # SPEAD stream control for "start" goes at end of header
    header_bytes += SPEAD_STREAM_START
    n_spead_items += 1

    # global spead header and the 4 required fields that follow
    global_hdr_bytes = SPEAD_PKT_HEADER + int.to_bytes(
        n_spead_items + 4, 2, byteorder="big"
    )
    # heap id
    global_hdr_bytes += spead_item_bytes(SPEAD_HEAP_CTR, heap_id)
    # heap size
    global_hdr_bytes += spead_item_bytes(SPEAD_HEAP_SIZ, len(payload_bytes))
    # heap offset (always start at beginning of heap = 0 ?)
    global_hdr_bytes += spead_item_bytes(SPEAD_HEAP_OFS, 0)
    # payload length bytes
    global_hdr_bytes += spead_item_bytes(SPEAD_PAY_LEN, len(payload_bytes))

    # offsets packetiser requires to overwrite data when using the packet as
    # a template for successive channels
    # offset of heap_id in spead_header 6 lines above
    # 15 = low byte, 10 = high byte.
    # FPGA wants to know where the Visibility Channel portion is
    all_offsets = {"heap_counter": 14}
    # adjust value offsets for the length of prepended SPEAD header
    all_offsets.update(
        {
            key: value + len(global_hdr_bytes)
            for key, value in value_offsets.items()
        }
    )

    return all_offsets, global_hdr_bytes + header_bytes + payload_bytes
