# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.


import numpy as np
from ska_low_cbf_fpga import FpgaPeripheral


class FpgaBeamDelaysPss:
    """
    An interface to PSS beam delay polynomial peripherals
        **** Peripheral Name: PSSbeamPolynomialsP_Y (P=0,1 Y=0,1,2,3) ****

    Pipelines (P) have 4 peripherals (Y) each holding a RAM block
    TODO FPGA RAM is curently sized for 16 beams, but 128 ultimately
    RAM contents are:
      48kwords polynomial coefficients buffer 0 (512stn x 16beam x 6word entry)
      48kwords polynomial coefficients buffer 1 (512stn x 16beam x 6word entry)
      2kword Sky frequency (1024vchan x  2word entry)

    Concept is that one polynomial buffer will be in active use and the other
    buffer can be updated with newer polynomial information.
    The start-of-validity and end-of-validity registers for the two polynomial
    buffers are not in this module. They are in the CT2 module.
    """

    NUM_BEAMFORMERS = 4
    BEAMS_PER_BEAMFORMER = 16  # TODO FPGA limitation: should be 128
    MAX_BEAMS = NUM_BEAMFORMERS * BEAMS_PER_BEAMFORMER

    POLY_BUF_OFFSET = 0
    WORDS_PER_POLY = 6  # 6 single precision floating-point coeffs per stn poly
    MAX_STATIONS_BEAMFORMED = 512  # Max that can be beamformed by a FPGA
    ODD_STN_OFFSET = (
        WORDS_PER_POLY * (MAX_STATIONS_BEAMFORMED // 2) * BEAMS_PER_BEAMFORMER
    )
    WORDS_PER_BEAM = (MAX_STATIONS_BEAMFORMED // 2) * WORDS_PER_POLY
    WORDS_PER_BUF = BEAMS_PER_BEAMFORMER * WORDS_PER_BEAM * 2

    FREQ_BUF_OFFSET = 96 * 1024

    def __init__(self, fpga_periphs: list[FpgaPeripheral], logger):
        self.regs = fpga_periphs  # Access to FPGA peripheral registers
        self.logger = logger
        assert (
            len(fpga_periphs) == self.NUM_BEAMFORMERS
        ), f"Expected {self.NUM_BEAMFORMERS}, got {len(fpga_periphs)}"

    def write_sky_freqs(self, freqs: list[float]) -> None:
        """
        Write sky frequency GHZ values for each station in use
        :param freqs: list of floating point frequencies in GHZ, one per vchan
        """
        frq_words = []
        # Two words per entry, second word always zero
        for frq in freqs:
            frq_words.extend([frq, 0.0])
        # Driver needs words as numpy array
        np_words = np.asarray(frq_words, dtype=np.float32)
        start = self.FREQ_BUF_OFFSET
        end = start + len(frq_words)
        # same sky GHz vchan info needed in each beamformer
        for bf_no in range(0, self.NUM_BEAMFORMERS):
            self.regs[bf_no].data[start:end] = np_words.view(np.uint32)

    def write_dly_for_beam(
        self,
        buf_no: int,
        beam_no: int,
        even_words: list[int],
        odd_words: list[int],
    ) -> None:
        """
        Write PST beam delay polynomial coefficients for one beam

        There are 6 floating-point coefficients (5th order polynomial)
        for each station contributing to the beam. To allow FPGA evaluation
        in parallel, the stations coefficients are divided into two groups
        by even or odd order of the station in the list of stations:
        1st station Group_A, 2nd station Group_B, 3rd station Group_A, ...

        There are two buffers holding a complete set of coefficients. One in
        current use, and the other that new coefficients can be written to.

        :param buf_no: which of the 2 buffers to use (0 or 1)
        :param beam_no: which beam number to write coefficients for [0..N]
        :param even_words: list of ints for even-numbered stations
        :param odd_words: list of intss for odd-numbered stations
        """
        assert 0 <= buf_no <= 1, f"No buffer number {buf_no}"
        assert (
            0 <= beam_no < self.MAX_BEAMS
        ), f"beam {beam_no} outside range [0..{self.MAX_BEAMS-1}]"

        # Stripe the beams across the beamformer FPGA units
        # EG Seven beams would be allocated:
        #  beamformer_0 calculates beams: 1 & 5
        #  beamformer_1 calculates beams: 2 & 6
        #  beamformer_2 calculates beams: 3 & 7
        #  beamformer_3 calculates beam : 4
        bf_no = beam_no % self.NUM_BEAMFORMERS
        entry_no = beam_no // self.NUM_BEAMFORMERS

        # Where in FPGA RAM block the even-station data goes
        start = (
            self.POLY_BUF_OFFSET
            + buf_no * self.WORDS_PER_BUF
            + entry_no * self.WORDS_PER_BEAM
        )
        end = start + len(even_words)
        np_even_words = np.asarray(even_words, dtype=np.uint32)
        self.regs[bf_no].data[start:end] = np_even_words

        # Where in FPGA RAM block the odd-station data goes
        start = (
            self.POLY_BUF_OFFSET
            + buf_no * self.WORDS_PER_BUF
            + entry_no * self.WORDS_PER_BEAM
            + self.ODD_STN_OFFSET
        )
        end = start + len(odd_words)
        np_odd_words = np.asarray(odd_words, dtype=np.uint32)
        self.regs[bf_no].data[start:end] = np_odd_words
