# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.
"""
This file contains the mapping between PSS signal processing stage names
and the python classes that interface the stages to FPGA peripheral registers

It also contains code to construct a array of objects representing the
signal processing pipelines in a FPGA from a definition of the signal
processing stages
"""

from logging import Logger

from ska_low_cbf_fpga import FpgaPeripheral

from ska_low_cbf_proc.icl.periph_beam_delays_pss import FpgaBeamDelaysPss
from ska_low_cbf_proc.icl.periph_beam_jones_pss import FpgaBeamJonesPss
from ska_low_cbf_proc.icl.periph_ct1_psr import FpgaCt1Psr
from ska_low_cbf_proc.icl.periph_ct2_pss import FpgaCt2Pss
from ska_low_cbf_proc.icl.periph_ingest import FpgaIngest
from ska_low_cbf_proc.icl.periph_packetiser_psr import FpgaPacketiserPsr
from ska_low_cbf_proc.icl.periph_station_jones_pss import FpgaStationJonesPss

SIGPROC_STAGE_NAME_MAP = {
    "ingest": FpgaIngest,
    "ct1_psr": FpgaCt1Psr,
    "ct2_pss": FpgaCt2Pss,
    "stnjones_pss": FpgaStationJonesPss,
    "beamdlys_pss": FpgaBeamDelaysPss,
    "beamjones_pss": FpgaBeamJonesPss,
    "packetiser_psr": FpgaPacketiserPsr,
}
"""
Dictionary that maps stage type-names to the class that is an interface
for to the peripherals in the stage  {stage_name: stage_iface_class, }
"""


def make_periph(stage_name, fpga_periph: FpgaPeripheral, logger: Logger):
    """
    Factory that makes an object that provides interfaces to a particular
    FPGA peripheral

    :arg stage_name: String containing name of stage. Name expected to
    be present in SIGPROC_STAGE_NAME_MAP
    :arg fpga_periph: driver used to access FPGA registers of peripheral device
    :arg logger: logging object
    :return: ojbect that interfaces to FPGA peripheral
    """
    if stage_name not in SIGPROC_STAGE_NAME_MAP:
        return None
    periph = SIGPROC_STAGE_NAME_MAP[stage_name](fpga_periph, logger)
    return periph


def make_periph_array(stage_name, fpgas: list[FpgaPeripheral], logger: Logger):
    """
    Factory creating an object that intefaces to an array of identical peripherals

    :arg stage_name: String containing name of peripheral. Name expected to
    be present in FPGA_PERIPHERAL_MAP
    :arg fpgas: drivers used to access FPGA registers of peripheral devices
    :arg logger: logging object
    :return: ojbect that interfaces to FPGA peripherals
    """
    if stage_name not in SIGPROC_STAGE_NAME_MAP:
        return None
    periph = SIGPROC_STAGE_NAME_MAP[stage_name](fpgas, logger)
    return periph


def make_pipelines(
    fpga,
    pipeline_def: list[
        dict,
    ],
    logger,
):
    """
    Make "pipeline" array corresponding to signal-processing pipelines
    in the FPGA.

    :param fpga: args FGPA object
    :param pipeline_def: structure holding definition of pipelines
        with names of each peripheral in each pipeline
    :param logger: python log writer object

    :return: List with one element per signal processing pipeline, and
    each list containing a dict of classes that provide access to each stage
    in the FPGA signal-processing pipeline
    """
    pipelines = []
    for stage_typename, args_names in pipeline_def.items():
        for idx, args_name in enumerate(args_names):
            if len(pipelines) < idx + 1:
                pipelines.append({})
            if isinstance(args_name, str):  # a single name
                if args_name not in fpga._peripherals:
                    txt = f"Missing {args_name} in FPGA map"
                    logger.warning(txt)
                    pipelines[idx][stage_typename] = None
                    continue
                p = make_periph(
                    stage_typename,
                    fpga._peripherals[args_name],
                    logger,
                )
            else:  # array of identical periphs managed by one object
                periph_group = []
                for name in args_name:
                    if name not in fpga._peripherals:
                        txt = f"Missing {name} in FPGA map"
                        logger.warning(txt)
                        periph_group.append(None)
                        continue
                    periph_group.append(fpga._peripherals[name])
                p = make_periph_array(
                    stage_typename,
                    periph_group,
                    logger,
                )
            pipelines[idx][stage_typename] = p

    # check all pipelines have the same signal-processing stages
    stage_names_ref = None
    for idx, pipe in enumerate(pipelines):
        stage_names = set(pipe.keys())
        if stage_names_ref is None:
            stage_names_ref = stage_names
        if stage_names != stage_names_ref:
            logger.error("Pipeline_%d spec wrong", idx)
    logger.info(
        "%d signal processing pipelines defined",
        len(pipelines),
    )

    return pipelines
