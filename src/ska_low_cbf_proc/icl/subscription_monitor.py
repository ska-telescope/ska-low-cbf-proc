class SubscriptionMonitor:
    """A simple convenience class to keep track of failed subscriptions"""

    def __init__(self):
        self._failed_delays = {}
        """Keeps track of failed delay polynomial subscriptions; format:
        { #subarray   beams
            1:        [1, 2, 3],
            3:        [7, 8],
            ...
        }
        """

    def add(self, subarray_id: int, beam_id: int) -> bool:
        """Make note of the specified subarray-beam.
        Return True if our records are modified; False otherwise.
        """
        if subarray_id not in self._failed_delays:
            self._failed_delays[subarray_id] = []
        if beam_id not in self._failed_delays[subarray_id]:
            self._failed_delays[subarray_id].append(beam_id)
            return True
        return False

    def remove(self, subarray_id: int, beam_id: int) -> bool:
        """Remove the specified subarray-beam.
        Return True if our records are modified; False otherwise.
        """
        if (
            subarray_id in self._failed_delays
            and beam_id in self._failed_delays[subarray_id]
        ):
            self._failed_delays[subarray_id].remove(beam_id)
            return True
        return False

    def is_ok(self, subarray_id: int, beam_id: int) -> bool:
        "Has the subscription for the specified subarray-beam failed?"
        return not (
            subarray_id in self._failed_delays
            and beam_id in self._failed_delays[subarray_id]
        )

    def get(self) -> dict:
        "Get the current state (dictionary)."
        return self._failed_delays
