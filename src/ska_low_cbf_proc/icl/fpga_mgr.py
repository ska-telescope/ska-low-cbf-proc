# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""Abstract FPGA Manager class"""

import logging
import queue
import time
from abc import ABC, abstractmethod
from threading import Thread
from typing import Callable

from ska_low_cbf_fpga import FpgaPersonality


class FpgaMgr(ABC):
    """
    Abstract class implementing thread logic for derived classes that
    monitor and program FPGA registers.

    Each different FPGA type (PST, Correlator) will have its own derived class
    to handle the specifics of the FPGA type. The derived classes will
    translate parameters specified by the Allocator into register updates and
    also periodically monitor FPGA registers that indicate the quality of the
    FPGA operations
    """

    PERIODIC_INTERVAL_SECS = 10.0
    """Interval at which periodic tasks are run"""

    def __init__(
        self,
        fpga: FpgaPersonality,
        fail_cbk: Callable[[], None],
        logger: logging.Logger,
    ):
        self._cmd_q = queue.Queue()  # queue for commands to manager
        self._fpga = fpga
        self._fail_cbk = fail_cbk
        self.logger = logger
        self._periodic_secs = self.PERIODIC_INTERVAL_SECS
        self._normal_exit = False
        self._thread = None

    def update_state(self, newstate):
        """
        Desired new state for FPGA.
        This method is called from outside the manager thread. The new state
        is placed in a queue for the fpga manager thread to read and implement
        """
        self._cmd_q.put(newstate)

    def stop_manager_thread(self):
        """
        Called from outside the manager thread to stop the thread
        Also waits for the thread to exit (function is blocked while waiting)
        """
        self._cmd_q.put({"keep_running": False})
        self._wait_for_exit()

    def _wait_for_exit(self):
        """
        Wait for the fpga manager thread to exit
        """
        self._thread.join()
        self._thread = None

    def start_manager_thread(self):
        """
        This method is called to run the fpga manager thread
        """
        self._thread = Thread(target=self._manager_thread)
        self._thread.start()

    def _manager_thread(self):
        """
        This method is run as a thread while any FPGA image is loaded.
        It ensures the manager thread is always running, restarting the
        manager thread in the event of an uncaught exception.

        If a non-None fail_cbk was passed, it checks for driver failure
        and invokes the callback if there are too many failures
        """
        fail_interval_secs = 60
        num_allowed_fails = 3
        restart_times = []

        while True:
            inner_thr = Thread(target=self._fpga_thread)
            inner_thr.start()
            inner_thr.join()
            inner_thr = None  # Required. Thread respawn needs fresh context
            if self._normal_exit:
                return

            # Unexpected exit from manager thread if we get here
            # Check to see if we have a FPGA driver failure indicated by
            # too many restarts in the past fail_interval_secs
            if self._fail_cbk is not None:
                now = time.time()
                restart_times.append(now)
                # only keep recent restart times
                while (
                    len(restart_times) > 0
                    and (now - restart_times[0]) > fail_interval_secs
                ):
                    restart_times.pop(0)
                # too many restarts in the time interval means driver failed
                if len(restart_times) > num_allowed_fails:
                    self.logger.error("FPGA driver failed")
                    self._fail_cbk()
                    return

            time.sleep(1.5)  # avoid possiblity of 100% CPU use
            self.logger.warning("Oops! Respawn FPGA manager thread")

    def _fpga_thread(self):
        """
        This method is run as a thread while any PST FPGA image is loaded
        The thread exits when FPGA image is unloaded, but is not intended
        to exit otherwise (if it dies, outer thread will respawn it)
        While running the thread is responsible for:
        - Real-time monitoring of some key FPGA register values
        - Register updates needed due to eg VCT changes
        """
        self.logger.info("Start of FPGA manger thread")
        self.logger.info(f"fpga={self._fpga} cmd_q={self._cmd_q}")

        last_update_time = time.time()
        while True:
            # Wait for command with timeout so periodic tasks run to schedule
            next_update_time = last_update_time + self._periodic_secs
            t_wait = next_update_time - time.time()
            t_wait = max(0, t_wait)  # wait must not be negative
            try:
                mgr_cmd = self._cmd_q.get(timeout=t_wait)
            except queue.Empty:
                # On timeout, run things we need to do periodically
                last_update_time = time.time()
                self.periodic_actions()
                continue

            # First: Stop this thread if we need to
            keep_running = mgr_cmd.get("keep_running", True)
            if not keep_running:
                self.cleanup()
                self._normal_exit = True
                break

            if not self._cmd_q.empty():
                # Discard cmd: later cmds have more recent registers/subarrays
                continue

            # Next: Handle register updates arising from the command params
            self.do_register_changes(mgr_cmd)

            # TODO Run periodic update after command??

        # TODO any register updates to shut down FPGA output
        self.logger.info("End of FPGA manager thread")

    @abstractmethod
    def do_register_changes(self, cmd):
        """
        Called to change FPGA register settings as a result of changes to
        allocator attributes specifying the FPGA state
        :param cmd: Dictionary of parameters to change
        """
        raise (NotImplementedError)

    @abstractmethod
    def periodic_actions(self):
        """
        Called to monitor fpga registers for performance, and (maybe) to apply
        periodic updates eg delay polynomials as register settings
        """
        raise (NotImplementedError)

    @abstractmethod
    def cleanup(self):
        """
        Final actions to be taken before thread exit
        """
        raise (NotImplementedError)
