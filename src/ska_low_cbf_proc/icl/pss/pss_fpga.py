# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.
"""
This file defines the ARGS peripherals that make up
each signal processing pipeline of the PSS FPGA
"""
from ska_low_cbf_fpga import FpgaPersonality

from ska_low_cbf_proc.icl.periphs import make_pipelines


class PssFpga(FpgaPersonality):
    """
    This class groups FPGA ARGS peripherals into signal-processing pipelines
    and exposes the pipelines via its two public attributes:

    - num_pipelines  : How many pipelines in the FPGA
    - pipelines      : Array (1 entry per pipeline) of dictionaries
                        containing interfaces to each stage of processing

    Operations on a peripheral in a pipeline have form eg:
        self.pipelines[0].ingest.clear_all_vcts()
    """

    # ARGs peripherals for each PSS pipeline stage
    #
    # key= Stage type name, used to look up class that interfaces to the peripheral
    #     (should exist in periphs.SIGPROC_STAGE_NAME_MAP
    #      and be implemented in periph_<key>.py)
    # value= List of either:
    #     ARGS peripheral names (one per signal processing pipeline)
    #     OR list of ARGS names of parallel peripherals in each pipeline

    PIPE_PERIPHS = {
        "ingest": ["lfaadecode100g", "lfaadecode100g_2"],
        "ct1_psr": ["pss_ct1", "pss_ct1_2"],
        "ct2_pss": ["ct2", "ct2_2"],
        "stnjones_pss": [
            "pssbeamformer0_stationjones",
            "pssbeamformer1_stationjones",
        ],
        "beamdlys_pss": [
            [
                "pssbeampolynomials0_0",
                "pssbeampolynomials0_1",
                "pssbeampolynomials0_2",
                "pssbeampolynomials0_3",
            ],
            [
                "pssbeampolynomials1_0",
                "pssbeampolynomials1_1",
                "pssbeampolynomials1_2",
                "pssbeampolynomials1_3",
            ],
        ],
        "beamjones_pss": [
            "pssbeamformer0_beamjones",
            "pssbeamformer1_beamjones",
        ],
        "packetiser_psr": ["packetiser", "packetiser_2"],
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Log names of peripherals in the FPGA map
        self._logger.info(
            "pss peripheral names: %s",
            str(sorted([name for name in self._peripherals])),
        )

        # Read num_pipelines from FPGA
        self.num_pipelines = self["system"][
            "no_of_bf_pipeline_instances"
        ].value

        # Make list of objects interfacing to signal-processing pipelines
        # Access each pipeline peripheral via:
        # self.pipelines[pipline_index][peripheral_class_name].method_name()
        self.pipelines = make_pipelines(self, self.PIPE_PERIPHS, self._logger)

    def __del__(self):
        "Cleanup as Alveo card may be left locked when using XRT driver"
        super().__del__()
