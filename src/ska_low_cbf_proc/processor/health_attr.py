# -*- coding: utf-8 -*-
#
# (c) 2024 CSIRO Astronomy and Space.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""
Abstract base class for processor health affecting attributes
"""

from abc import ABC
from time import time

from ska_control_model import HealthState
from tango import AttrQuality


class HealthAttr(ABC):
    """
    Base class for calculation of contribution to health Tango attribute values

    Objects of this class have two important python (not Tango!) attributes
    - "health"  :  Holds a SKA HealthState value
    - "quality" :  Holds a Tango AttrQuality value
    These values can be used to construct a reply to a Tango Attribute read
    An alternative way to construct a Tango Attribute read reply is to use
    the value_date_quality() convenience method which returns a tuple
    containing health, quality, and additionally a modification time

    To update quality, Clients using this class should
    1. set the quality value    ( obj.quality = AttrQuality.ATTR_XXXX )
    2. call update_health       ( obj.update_quality() )
    --- OR, if qual_updater was initialised non-null --
    1. call update_quality      ( obj.update_quality() )

    To update the value, Clients using this class should
    1. call set_value           ( obj.set_value(new_value) )

    The class also supports the concept of value overrides, but how to use ??
    """

    def __init__(self, name: str, qual_updater=None):
        """
        :param name: health attribute name
        :param qual_updater: a function to call when updating quality factor;
            probably depends on factors beyond the scope of this class
            If present, the function must return a Tango AttrQuality ENUM value
        """
        self.name = name
        self.quality = AttrQuality.ATTR_INVALID
        self.true_value = None  # actual measured value regardless of override
        self.is_override = False  # Tango attribute override in effect
        self.override_value = None  # stores value while override is in force
        self.mod_time = 0.0
        self.health = HealthState.OK
        self.quality_updater = qual_updater

    def update_health(self) -> None:
        """Override in a derived class

        Recalculate the health contribution of this attribute based on its
        current value and quality factor"""

    def run_checker(self, value):
        """Override in a derived class

        Make some calculations based on the current attribute reading and
        possibly update health condition..
        """

    def update_quality(self) -> bool:
        """Refresh AttrQuality for this attribute based on some external
        factor encapsulated in quality_updater member

        :return: True if quality factor has changed, False otherwise
        """
        if self.quality_updater:
            qual = self.quality_updater()
            if self.quality != qual:
                self.quality = qual
                self.update_health()
                return True
        return False

    def set_value(self, value) -> bool:
        """Set this attribute's value.
        If Tango attribute override is in effect only updates to override_value
        are taking place.

        :param value: either current (true) value or override value when
                      is_override == True
        :return: True if the value has changed, False otherwise
        """
        if self.override_value != value:
            self.override_value = value
            self.mod_time = time()
            if not self.is_override:
                self.true_value = value
            self.update_health()
            return True
        return False

    @property
    def value(self):
        "Return the true/override value depending on override state"
        return self.override_value if self.is_override else self.true_value

    # FIXME add setter?

    @property
    def value_date_quality(self) -> tuple:
        """Return this attribute's (value, modification time, quality) tuple
        Used by Tango attributes that care about attribute's quality.
        """
        return self.value, self.mod_time, self.quality
