# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Astronomy and Space.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""
Code related to checking FPGA registers for values that indicate device
health issues.
"""

import importlib.resources
import time
from threading import Event, Thread

import yaml
from ska_low_cbf_fpga import XrtInfo


class HwHealth:
    """
    Class that periodically reads Alveo Hardware info relevant to health
    evaluation via an ``XrtInfo`` object.
    """

    def __init__(self, mon_file, xrt_info: XrtInfo, mon_update_cbk, logger):
        """
        Initialisation

        :param mon_name: name of monitoring params to use from resources yaml
        :param mon_update_cbk: callable function accepts dict of mon values
        """
        self._logger = logger
        self._xrt_info = xrt_info
        self._thread = None
        self._stop_running = None
        self._poll_secs = 0
        self._mon_update_cbk = mon_update_cbk
        self._next_update_time = None

        yaml_str = importlib.resources.read_text(
            "ska_low_cbf_proc.resources", mon_file
        )
        self._mon_point_info = (
            yaml.load(yaml_str, Loader=yaml.Loader)["monitoring_points"] or {}
        )
        logger.info("monitoring_points: %s", str(self._mon_point_info))

    def get_hwmon_lims(self) -> dict:
        """
        Get max/min limits for each monitored item
        """
        limits = {}
        for hw_param_name, cfg in self._mon_point_info.items():
            if hasattr(self._xrt_info, hw_param_name):
                limits[hw_param_name] = cfg
            else:
                self._logger.warning(f"Don't have {hw_param_name}")
        return limits

    def start_hw_monitoring(self, poll_secs) -> None:
        """
        Start hardware monitoring thread

        :param poll_seconds: time between polls
        """
        self._poll_secs = poll_secs
        self._thread = Thread(
            target=self._periodic_update,
            args=(),
        )
        self._stop_running = Event()
        self._thread.start()

    def stop_hw_monitoring(self) -> None:
        """Cease hardware monitoring"""
        self._stop_running.set()
        self._thread.join()

    def _periodic_update(self) -> None:
        """Thread routine to periodically check hw state"""
        self._logger.info("Start hardware health monitoring")
        # ongoing periodic reading of health-related registers
        self._next_update_time = time.time()  # First check immediately
        while True:
            wait_time = max(0, self._next_update_time - time.time())
            self._stop_running.wait(timeout=wait_time)
            if self._stop_running.is_set():
                break
            self._next_update_time += self._poll_secs

            # read all monitoring point values
            monpt_vals = {}
            for param_name, min_max in self._mon_point_info.items():
                if hasattr(self._xrt_info, param_name):
                    value = getattr(self._xrt_info, param_name).value
                    # round up to 1 decimal place to eliminate small noise
                    monpt_vals[param_name] = round(value, 1)
                else:
                    self._logger.warning(f"Don't have {param_name}")
            self._mon_update_cbk(monpt_vals)

        # clean up when health monitoring is stopped
        self._stop_running = None
        self._logger.info("Stopped monitoring hardware health")
