# -*- coding: utf-8 -*-
#
# (c) 2024 CSIRO Astronomy and Space.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""
Code related to checking parameters that indicate processor device's processing
health.
"""

import json
from importlib.resources import files
from threading import Lock, Thread
from time import sleep

import yaml
from ska_control_model import HealthState
from tango import AttrQuality

from .health_attr import HealthAttr

# from time import time # UNCOMMENT when SKABaseDevice is upgraded


ATTR_DELAY_SUBSCR_OK = "process_delay_subscription_ok"
ATTR_DELAY_VALID = "process_delay_poly_valid"
ATTR_SPEAD_PACKETS = "process_spead_packets_ok"

# a list of all attributes, often used in loops:
ALL_ATTRS = (ATTR_DELAY_VALID, ATTR_DELAY_SUBSCR_OK, ATTR_SPEAD_PACKETS)
DEFAULT_PERIOD_SEC = 4  # how often to poll health indicators


class DelayPolyValid(HealthAttr):
    """Keep track of delay polynomial validity status. The information about
    delay polynomial validity is periodically updated from a thread within
    ProcessorComponentManger and relayed to `ProcessingHealth.attr_update`
    method.
    Based on the delay poly. validity the `health` attribute holds the value
    that contributes to the overall "processing" health status.
    The `quality` attribute is `ATTR_VALID` when the subarray is scanning and
    only in such a case the `health` attribute counts towards overall
    "processing" health status.
    """

    def __init__(self, name: str):
        super().__init__(name, None)
        self.true_value = False  # True=delay polys are valid, False otherwise

    def update_health(self) -> None:
        """Recalculate the health contribution of this attribute based on its
        current value and quality factor"""
        self.health = (
            HealthState.OK
            if self.quality == AttrQuality.ATTR_VALID and self.value
            else HealthState.FAILED
        )


class SpeadPacketCounter(HealthAttr):
    "Monitor SPEAD packets arrival while subarray is scanning"

    def __init__(self, name: str):
        super().__init__(name, None)
        self.true_value = (
            False  # True when counter incrementing, False otherwise
        )
        self._previous_count = None  # inital (invalid) counter value

    def update_health(self) -> None:
        """Recalculate the health contribution of this attribute based on its
        current value and quality factor"""
        self.health = (
            HealthState.OK
            if self.quality == AttrQuality.ATTR_VALID and self.value
            else HealthState.FAILED
        )

    def run_checker(self, value) -> bool:
        """Check if the packet counter increments and update this
        monitoring point health accordingly
        """
        if self._previous_count is None:
            # ignore the 1st FPGA register reading as it's inconclusive
            self._previous_count = value
            return False
        if incrementing := value != self._previous_count:
            self._previous_count = value
        return (
            self.set_value(incrementing)
            and self.quality == AttrQuality.ATTR_VALID
        )


class ProcessingHealth:
    """
    Tracks components that contribute to "processing" health (parameters
    that indicate some state while data is flowing throught the system).
    Those "components" are objects of `HealthAttr`-derived class and they
    maintain their own health contribution (which is NOT a Tango attribute)
    to overall `ProcessingHealth` category/class' health.

    This class' `health` member value is read and used by `LowCbfProcessor`
    `health_process` Tango attribute - either when Tango clients are reding
    it or when recalculating `LowCbfProcessor.healthState` Tango attribute.

    See https://developer.skao.int/projects/ska-low-cbf/en/latest/health.html#health-of-low-cbf-hardware-devices
    """

    def __init__(self, cfg_file, proc_device, logger):
        """
        Initialisation

        :param proc_device: LowCfbProcessor device server
        :param logger: logger for diagnostic messages
        """
        self._proc_device = proc_device
        self._logger = logger
        self.health = HealthState.OK
        self.subarr_scanning = {}
        self._first_time = True
        # `-> needed to recalculate overall health even when there are no
        #     changes to initial conditions
        self._lock = Lock()
        # `-> need a lock: override_change() runs from Tango client thread and
        #     attr_update() runs from processor thread

        # delay polynomial related attributes:
        self.process_delay_poly_valid = DelayPolyValid(ATTR_DELAY_VALID)
        self.process_delay_subscription_ok = DelayPolyValid(
            ATTR_DELAY_SUBSCR_OK
        )
        # SPEAD packet counter related attribute:
        self.process_spead_packets_ok = SpeadPacketCounter(ATTR_SPEAD_PACKETS)
        # fetch settings from configuration file
        text = (
            files("ska_low_cbf_proc.resources").joinpath(cfg_file).read_text()
        )
        cfg = yaml.load(text, Loader=yaml.Loader)
        self._fpga_registers = cfg.get("fpga_registers", {})
        logger.info("PROCESS fpga_registers: %s", str(self._fpga_registers))
        period = cfg.get("check_period", DEFAULT_PERIOD_SEC)
        self._thread = Thread(target=self._periodic_update, args=(period,))
        self._thread.daemon = True
        self._thread.start()

    def subarray_event(self, evt_json: str) -> None:
        """
        Called when subarray.obsState (scanning) changes.

        :param evt_json: JSON string with delay polynomial validity for
                         station and PST/PSS beams
        """
        if evt_dict := json.loads(evt_json):
            for sub_id, val in evt_dict.items():
                if "scanning" in val:
                    is_scanning = val["scanning"]
                    self.subarr_scanning[sub_id] = is_scanning
                    qual = (
                        AttrQuality.ATTR_VALID
                        if is_scanning
                        else AttrQuality.ATTR_INVALID
                    )
                    for name in ALL_ATTRS:
                        attr = getattr(self, name)
                        attr.quality = qual
                        self._push_change(attr)
                        attr.update_health()
                else:
                    self._logger.warning("Malformed data")
        else:
            # we get an empty JSON at the end of scan, that renders attribute
            # quality INVALID, consequently subscribers will want to know that
            self.subarr_scanning = {}
            for name in ALL_ATTRS:
                attr = getattr(self, name)
                attr.quality = AttrQuality.ATTR_INVALID
                attr.update_health()
                self._push_change(attr)
        self.summarise_health()

    def attr_update(self, delays: dict) -> None:
        """
        Update state of delays - valid and subscription ok

        :param delays: a dictionary with status of station/PST/PSS beam delays
        """
        changed = self._first_time
        for entry in delays:
            if "subarray_id" not in entry:
                self._logger.error("Malformed data")
                continue
            sub_id = entry["subarray_id"]
            # values won't matter unless subarray is SCANNING
            if (sub_id in self.subarr_scanning) and not self.subarr_scanning[
                sub_id
            ]:
                continue

            # determine new attribute values
            delay_valid, subscr_valid = True, True
            beams = entry["beams"]
            for stn_beam in beams:
                delay_valid &= stn_beam["valid_delay"]
                subscr_valid &= stn_beam["subscription_valid"]
                # handle PST/PSS beam delays
                for psx in ("pst", "pss"):
                    if psx not in stn_beam:
                        continue
                    for psx_beam in stn_beam[psx]:
                        delay_valid &= psx_beam["valid_delay"]
                        subscr_valid &= psx_beam["subscription_valid"]

            # apply attribute values
            with self._lock:
                for name, value in (
                    (ATTR_DELAY_VALID, delay_valid),
                    (ATTR_DELAY_SUBSCR_OK, subscr_valid),
                ):
                    attr = getattr(self, name)
                    if attr.set_value(value):
                        self._push_change(attr)
                        changed = True

        if changed:
            self.summarise_health()
        self._first_time = False

    def summarise_health(self) -> None:
        """Recalculate health_function Tango attribute based on its
        constituent components"""
        health = HealthState.OK
        # add other diagnostic points as we introduce them:
        for name in ALL_ATTRS:
            attr = getattr(self, name)
            if attr.quality != AttrQuality.ATTR_INVALID:
                health = max(health, attr.health)

        if self.health != health:
            self.health = health
            self._proc_device.push_change_event("health_process", health)
            self._proc_device.push_archive_event("health_process", health)
            self._proc_device.summarise_health_state()
            self._logger.info(f"PUSH health_process {health}")

    def override_change(self):
        """Called when Tango attribute override changes. If applicable we'll
        recalculate "processing" health category state.
        """
        with self._lock:
            changed = False
            for name in ALL_ATTRS:
                override = self._proc_device.get_override(name)
                attr = getattr(self, name)
                attr.is_override = override is not None
                old_health = attr.health
                if attr.is_override:
                    attr.set_value(override)
                attr.update_health()
                if old_health != attr.health:
                    self._push_change(attr)
                    changed = True
            if changed:
                self.summarise_health()

    def _periodic_update(self, check_period: int) -> None:
        """Thread loop: periodically check health indicators"""
        self._logger.info("Monitor 'PROCESSING' health")
        comp_mgr = self._proc_device.component_manager
        while True:
            # check all FPGA based health indicators
            changed = False
            for attr_name, cfg in self._fpga_registers.items():
                attr = getattr(self, attr_name, None)
                # do nothing until FPGA firmware is loaded
                if not self._proc_device.is_fw_loaded:
                    # in case subarray just STOPPED scanning:
                    if attr.update_quality():
                        self._push_change(attr)
                        changed = True
                    break
                if attr is None:
                    self._logger.warning(f"Don't have {attr_name}")
                    continue
                try:
                    periph, reg = cfg["peripheral"], cfg["register"]
                    value = comp_mgr.read_peripheral_register(periph, reg)
                    with self._lock:
                        if attr.run_checker(value):
                            self._push_change(attr)
                            changed = True
                except Exception as excpt:
                    self._logger.error(excpt)
            if changed:
                self.summarise_health()
            sleep(check_period)

    def _push_change(self, attr: HealthAttr) -> None:
        """Notify subscribers and archiver the attribute has changed

        :param attr: HealthAttr that has changed either value or AttrQuality
        """
        # FIXME once we upgrade Tango push AttrQuality as well
        #       the version of SKABaseDevice we use does not support it
        # self._proc_device.push_change_event(attr.name, attr.value, time(), attr.quality)
        # self._proc_device.push_archive_event(attr.name, attr.value, time(), attr.quality)
        self._proc_device.push_change_event(attr.name, attr.value)
        self._proc_device.push_archive_event(attr.name, attr.value)
        self._logger.info(
            f"PUSH CHANGE {attr.name} {attr.value} Q:{attr.quality}"
        )
