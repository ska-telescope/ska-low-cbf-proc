# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""Classes that manage FPGA download state and register updates."""

import logging
from abc import ABC
from enum import IntEnum
from queue import Queue
from threading import Lock, Thread  # , Timer

PROCESS_EVTS_IN_CALLER_CONTEXT = False


class Evt(IntEnum):
    """Events relevant to fpga download states states."""

    ON_ENTRY = 0
    ON_EXIT = 1
    NEW_ALVEO_INFO = 2
    NEW_SUBARRAY_INFO = 3
    TIMEOUT = 4
    DOWNLOAD_OK = 5
    DOWNLOAD_FAIL = 6
    ADMIN_ENABLE = 7
    DRIVER_FAIL = 8


class EvtHndlr(ABC):
    """Base Event handling logic."""

    def __init__(self, logger: logging.Logger):
        """
        Create an Event Handler.

        :param logger: for processing log messages.
        """
        self._cur_state = None
        self._evt_queue = Queue()
        self._logger = logger
        self._lock = Lock()  # protects self._is_processing
        self._is_processing = False

    def initial_state(self, initial_state):
        """Set starting state for handler."""
        self.transition(initial_state)
        self._process_evts()

    def transition(self, next_state):
        """Transition from one event handling state to another."""
        if self._cur_state is not None:
            self._cur_state(Evt.ON_EXIT, None)
        self._cur_state = next_state
        self._cur_state(Evt.ON_ENTRY, None)

    def add_evt(self, evt, evt_data):
        """Add an event to the queue of events to be processed."""
        self._evt_queue.put((evt, evt_data))
        # Ensure only one thread of execution processes events
        # at a time to get run-to-completion in-order behaviour
        with self._lock:
            if self._is_processing:
                return
            self._is_processing = True

        if PROCESS_EVTS_IN_CALLER_CONTEXT:
            # This can lead to deadlocks if caller is Tango event handler
            self._process_evts()
        else:
            # Use separate thread of execution to process queued events
            thr = Thread(
                target=self._process_evts,
                args=(),
            )
            thr.start()

    def _process_evts(self):
        """Process all events in queue until queue is empty."""
        self._lock.acquire()
        while not self._evt_queue.qsize() == 0:
            self._lock.release()

            # get next event & run current state's handling for the event
            evt, evt_data = self._evt_queue.get()
            self._cur_state(evt, evt_data)

            self._lock.acquire()
        self._is_processing = False
        self._lock.release()


class AlveoState(EvtHndlr):
    """
    State-machine Class handling state of Alveo firmware and FPGA registers
    """

    def __init__(
        self,
        fw_load_cbk,
        fw_unload_cbk,
        fw_start_cbk,
        fw_stop_cbk,
        fw_params_cbk,
        logger,
    ):
        """
        Create a Alveo state representation that manages downloading and
        transitions from in-use to not-in use.

        :param fw_load_cbk: callable, loads fpga bitfile returns success, arg=fw_name
        :param fw_unload_cbk: callable, unloads fpga bitfile
        :param fw_start_cbk: callable, starts fpga processig, arg=fw_name
        :param fw_stop_cbk: callable, stops fpga processing (no args)
        :param fw_params_cbk: callable, change registers of FPGA, two args:
            register_settings, subarray_info
        :param logger: for processing log messages.
        """
        super().__init__(logger)

        self._fw_load_cbk = fw_load_cbk
        self._fw_unload_cbk = fw_unload_cbk
        self._fw_start_cbk = fw_start_cbk
        self._fw_stop_cbk = fw_stop_cbk
        self._fw_params_cbk = fw_params_cbk

        self._current_firmware = None  # Latest firmware for this Alveo card
        self._current_fpga_regs = None  # Latest register state for this FPGA
        self._current_subarrays = None  # Latest configuration of all subarrays
        self._fpga_admin_enable = True  # AdminMode master switch

        self._download_success = False
        self._fw_changed_during_download = (
            False  # FW was changed during download
        )
        self._disable_in_download = False  # disabled during download

        # Start in "idle state"
        self.initial_state(self._state_idle)
        logger.info("Fpga_downloader state machine init complete")

    # Methods to handle events for each state

    def _state_idle(self, evt: Evt, evt_data):
        """Handle events in idle state."""
        if evt == Evt.ON_ENTRY:
            # nothing to do
            self._logger.info("FPGA enters idle state")
            return
        if evt == Evt.NEW_ALVEO_INFO:
            self._current_firmware = evt_data["fw"]
            self._current_fpga_regs = evt_data["regs"]
            if self._fpga_admin_enable and self._current_firmware is not None:
                self.transition(self._state_downloading)
            return
        if evt == Evt.NEW_SUBARRAY_INFO:
            self._current_subarrays = evt_data
            return
        if evt == Evt.ADMIN_ENABLE:
            self._logger.info("FPGA admin_enable set to %s", str(evt_data))
            self._fpga_admin_enable = evt_data
            if self._fpga_admin_enable and self._current_firmware is not None:
                self.transition(self._state_downloading)
            return
        if evt == Evt.ON_EXIT:
            # nothing to do
            self._logger.info("FPGA exit idle state")
            return
        # log unexpected evts: TIMEOUT, DOWNLOAD_OK, DOWNLOAD_FAIL, DRIVER_FAIL
        self._logger.error("IDLE FPGA: Unexpected evt_%d", evt)

    def _state_downloading(self, evt: Evt, evt_data):
        """Handle events while in downloading state"""
        if evt == Evt.ON_ENTRY:
            self._fw_changed_during_download = False
            self._download_success = False
            self._logger.info("FPGA enters downloading state")
            thr = Thread(
                target=self._download_monitor,
                args=(self._current_firmware,),
            )
            thr.start()
            return
        if evt == Evt.NEW_ALVEO_INFO:
            new_fw = evt_data["fw"]
            self._fw_changed_during_download = new_fw != self._current_firmware
            self._current_firmware = new_fw
            self._current_fpga_regs = evt_data["regs"]
            return
        if evt == Evt.NEW_SUBARRAY_INFO:
            self._current_subarrays = evt_data
            return
        if evt == Evt.ADMIN_ENABLE:
            self._logger.info(
                "FPGA admin_enable set to %s while downloading", str(evt_data)
            )
            self._fpga_admin_enable = evt_data
            return
        if evt == Evt.TIMEOUT:
            self._fw_unload_cbk()  # for completeness, probably no fw
            if not self._fpga_admin_enable:
                self.transition(self._state_idle)
                return
            if self._fw_changed_during_download:
                if self._current_firmware is None:
                    self.transition(self._state_idle)
                else:
                    self.transition(self._state_downloading)
                return
            self.transition(self._state_idle)
            return
        if evt == Evt.DOWNLOAD_OK:
            if not self._fpga_admin_enable:
                self._fw_unload_cbk()
                self.transition(self._state_idle)
                return
            if self._fw_changed_during_download:
                if self._current_firmware is None:
                    self.transition(self._state_idle)
                else:
                    self.transition(self._state_downloading)
                return
            self.transition(self._state_fpga_active)
            return
        if evt == Evt.DOWNLOAD_FAIL:
            self._fw_unload_cbk()  # for completeness, probably no fw
            if not self._fpga_admin_enable:
                self.transition(self._state_idle)
            if self._fw_changed_during_download:
                if self._current_firmware is None:
                    self.transition(self._state_idle)
                else:
                    self.transition(self._state_downloading)
                return
            self.transition(self._state_idle)
            return
        if evt == Evt.ON_EXIT:
            self._logger.info("FPGA exit downloading state")
            return
        # log unexpected events: driver_fail
        self._logger.error("DOWNLOAD FPGA: Unexpected evt_%d", evt)

    def _state_fpga_active(self, evt: Evt, evt_data):
        """Handle Events received while FPGA is loaded & active."""
        if evt == Evt.ON_ENTRY:
            self._logger.info("FPGA enter active state")
            self._logger.info("Start FPGA register-monitor thread")
            self._fw_start_cbk(self._current_firmware)
            self._logger.info("Apply register settings")
            self._fw_params_cbk(
                self._current_subarrays, self._current_fpga_regs
            )
            return
        if evt == Evt.NEW_ALVEO_INFO:
            new_fw = evt_data["fw"]
            changed_fw = new_fw != self._current_firmware
            self._current_firmware = new_fw
            self._current_fpga_regs = evt_data["regs"]
            if self._current_firmware is None:
                self.transition(self._state_idle)
                return
            if changed_fw:
                self.transition(self._state_downloading)
                return
            self._logger.info("Apply register settings")
            self._fw_params_cbk(
                self._current_subarrays, self._current_fpga_regs
            )
            return
        if evt == Evt.NEW_SUBARRAY_INFO:
            self._current_subarrays = evt_data
            self._logger.info("Apply register settings")
            self._fw_params_cbk(
                self._current_subarrays, self._current_fpga_regs
            )
            return
        if evt == Evt.ADMIN_ENABLE:
            self._logger.info(
                "FPGA admin_enable set to %s while in use", str(evt_data)
            )
            self._fpga_admin_enable = evt_data
            if not self._fpga_admin_enable:
                self.transition(self._state_idle)
            return
        if evt == Evt.DRIVER_FAIL:
            self._logger.error("FPGA driver failure detected")
            # Goto idle so it's possible to load a different FPGA
            self.transition(self._state_idle)
            return
        if evt == Evt.ON_EXIT:
            self._logger.info("Stop FPGA register-monitor thread")
            self._fw_stop_cbk()
            self._logger.info("Unload FPGA firmware")
            self._fw_unload_cbk()
            self._logger.info("FPGA exit active state")
            return
        # log unexpected events: TIMEOUT, DOWNLOAD_OK, DOWNLOAD_FAIL
        self._logger.error("LOADED FPGA: Unexpected evt_%d", evt)

    def _download_monitor(self, fw_name):
        """
        Monitor the thread that downloads firmware. this can take minutes
        to complete and should be run in a thread

        This code must never crash, but the downloader thread that it
        launches may crash. It must always create a success or fail event
        """
        self._download_success = False
        thr = Thread(
            target=self._download_task,
            args=(fw_name,),
        )
        self._logger.info("FW download start")
        thr.start()
        # wait for download task to succeed or fail. Task updates
        # self._download_success if it completes without crashing.
        # join returns when completed or crashed
        thr.join()

        self._logger.info("FW download ok=%s", str(self._download_success))
        if self._download_success:
            self.add_evt(Evt.DOWNLOAD_OK, None)
            return
        self.add_evt(Evt.DOWNLOAD_FAIL, None)
        return

    def _download_task(self, fw_name):
        """
        Perform firmware image download and FPGA bitfile programming
        This can take minutes to complete, must be run in a thread
        to avoid blocking Tango
        """
        # TODO: modify cbk to allow downloader to indicate timeout as well?
        self._download_success = self._fw_load_cbk(fw_name)

    # === Interface functions used by external callers ===

    def subarray_update(self, subarray_info: dict) -> None:
        """
        Generate event for statemachine indicating subarray configuration info
        has changed

        :param subarray_info: dictionary containing configuration of all
        subarrays that are currently configured (incl whether scanning)
        """
        self.add_evt(Evt.NEW_SUBARRAY_INFO, subarray_info)

    def register_update(self, register_info: dict) -> None:
        """
        Generate event for statemachine indicating desired register settings
        for the FPGA have changed

        :param register_info: register info for this FPGA. Dictionary has
        "fw" key specifying type of firmware ("pst", "vis", )
        "regs" key with info about FPGA register settings, format depends
        on the type of FPGA loaded
        """
        self.add_evt(Evt.NEW_ALVEO_INFO, register_info)

    def fpga_admin_enable(self, enable: bool) -> None:
        """
        Generate event for statemachine indicating request to administratively
        enable/disable FPGA operation

        :param enable: boolean, true for enabled, false for disabled
        """
        self.add_evt(Evt.ADMIN_ENABLE, enable)

    def fpga_driver_fail(self) -> None:
        """
        Generate statemachine event indicating FPGA driver has failed
        """
        self.add_evt(Evt.DRIVER_FAIL, None)

    def is_fpga_used(self) -> bool:
        """
        Determine if FPGA is in use
        :return: boolean
        """

        return self._cur_state != self._state_idle
