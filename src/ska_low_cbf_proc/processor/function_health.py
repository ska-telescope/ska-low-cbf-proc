# -*- coding: utf-8 -*-
#
# (c) 2024 CSIRO Astronomy and Space.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""
Code related to checking parameters that indicate device's functional health.
"""

from importlib.resources import files
from threading import Event, Lock, Thread

import yaml
from ska_control_model import HealthState
from tango import AttrQuality

from .health_attr import HealthAttr

# UNCOMMENT once SKABaseDevice is updted, see _push_change below
# from time import time


ATTR_FW_LOADED = "function_firmware_loaded"
ATTR_DRIVER_OK = "function_driver_ok"


class DriverOkAttr(HealthAttr):
    "Handle XRT driver ok health attribute state"

    def __init__(self, name, qual_updater):
        super().__init__(name, qual_updater)
        self.true_value = False
        self._previous_uptime = 0

    def update_health(self) -> None:
        """Recalculate the health contribution of this attribute based on its
        value and current quality factor"""
        self.health = (
            HealthState.OK
            if self.quality == AttrQuality.ATTR_VALID and self.value
            else HealthState.FAILED
        )

    def run_checker(self, value) -> bool:
        """Check if FPGA `uptime` value has changed from previous poll

        :param value: current `uptime` value
        :return: True if the value has changed, False otherwise
        """
        if changed := value != self._previous_uptime:
            self._previous_uptime = value
        return changed


class FunctionHealth:
    """
    Periodically monitor indicators of processor's 'functional' health and
    report back via a callback.
    """

    def __init__(self, cfg_file: str, proc_device, logger):
        """
        Initialisation

        :param cfg_file: file name containing monitoring params
        :param proc_device: LowCfbProcessor device server
        :param logger: logger used for diagnostic messages
        """
        self._proc_device = proc_device
        self._logger = logger
        self._thread = None
        self._lock = Lock()
        self._stop_running = None
        self.health = HealthState.OK
        setattr(
            self,
            ATTR_DRIVER_OK,
            DriverOkAttr(ATTR_DRIVER_OK, self._is_driver_attr_valid),
        )
        text = (
            files("ska_low_cbf_proc.resources").joinpath(cfg_file).read_text()
        )
        self._fpga_registers = (
            yaml.load(text, Loader=yaml.Loader)["fpga_registers"] or {}
        )
        logger.info("fpga_registers: %s", str(self._fpga_registers))

    def start_monitoring(self, check_period: int) -> None:
        """
        Start monitoring thread

        :param check_period: check period in seconds
        """
        self._thread = Thread(
            target=self._periodic_update, args=(check_period,)
        )
        self._stop_running = Event()
        self._thread.start()

    def stop_monitoring(self) -> None:
        """Stop function health monitoring"""
        self._stop_running.set()
        self._thread.join()

    def _is_driver_attr_valid(self) -> AttrQuality:
        """Used to establish if XRT driver_ok attribute is valid; depends on external
        state (firmware loaded)"""
        return (
            AttrQuality.ATTR_VALID
            if self._proc_device.is_fw_loaded
            else AttrQuality.ATTR_INVALID
        )

    def _periodic_update(self, check_period: int) -> None:
        """Thread loop periodically checking health indicators"""
        self._logger.info("Start function health monitoring")
        sleep_time = 0  # first run -  check immediately
        comp_mgr = self._proc_device.component_manager
        while True:
            self._stop_running.wait(sleep_time)
            if self._stop_running.is_set():
                break
            # check all health indicators
            readings = {}
            for attr_name, cfg in self._fpga_registers.items():
                attr = getattr(self, attr_name)
                # push event when there's a CHANGE in AttrQuality
                if attr.update_quality():
                    self._push_change(attr)
                    self.summarise_func_health()
                # nothing to do unless FW is loaded
                if not self._proc_device.is_fw_loaded:
                    break
                try:
                    periph = cfg["peripheral"]
                    reg_name = cfg["register"]
                    reg_value = comp_mgr.read_peripheral_register(
                        periph,
                        reg_name,
                    )
                    readings.update({attr_name: attr.run_checker(reg_value)})
                except Exception as excpt:
                    self._logger.error(excpt)
                    # something's wrong with reading FPGA registers -
                    # indicate this throught the relevant Tango attribute
                    readings.update({ATTR_DRIVER_OK: False})
            if readings:
                self.attr_update(readings)
            sleep_time = check_period

        # clean up when health monitoring is stopped
        self._logger.info("Stopped monitoring functions health")
        self._stop_running = None

    def attr_update(self, values: dict = {}) -> None:
        """
        Update function health attributes values

        :param values: dict key=monitoring point name, value=current reading
        """
        # lock: acessed locally and from Tango attribute (attr_override) change
        with self._lock:
            if not values:
                # called from processor device on attr_override change
                # use current values and apply new overrides
                for name in (ATTR_DRIVER_OK,):
                    if attr := getattr(self, name, None):
                        values.update({attr.name: attr.true_value})
            something_changed = False
            for name, value in values.items():
                override = self._proc_device.get_override(name)
                if override is not None:
                    value = override
                if attr := getattr(self, name, None):
                    attr.is_override = override is not None
                else:
                    self._logger.warning(f"Don't have {name}")
                    continue
                # check value/quality change but ensure they both run
                something_changed |= attr.update_quality()
                something_changed |= attr.set_value(value)
                if something_changed:
                    self._push_change(attr)

            if something_changed:
                self.summarise_func_health()

    def summarise_func_health(self) -> None:
        """Recalculate health_function Tango attribute based on its
        constituent components"""
        health = HealthState.OK
        # add other diagnostic points as we introduce them:
        for name in (ATTR_DRIVER_OK,):
            if attr := getattr(self, name, None):
                if attr.quality != AttrQuality.ATTR_INVALID:
                    health = max(health, attr.health)
            else:
                self._logger.error(f"Don't have {name} attribute")

        if self.health != health:
            self.health = health
            self._proc_device.push_change_event("health_function", health)
            self._proc_device.push_archive_event("health_function", health)
            self._proc_device.summarise_health_state()

    def _push_change(self, attr: HealthAttr) -> None:
        """Notify subscribers and archiver the attribute has changed

        :param attr: HealthAttr that has changed either value or AttrQuality
        """
        # FIXME once we upgrade Tango push AttrQuality as well
        #       the version of SKABaseDevice we use does not support it
        # self._proc_device.push_change_event(attr.name, attr.value, time(), attr.quality)
        # self._proc_device.push_archive_event(attr.name, attr.value, time(), attr.quality)
        self._proc_device.push_change_event(attr.name, attr.value)
        self._proc_device.push_archive_event(attr.name, attr.value)
        self._logger.info(
            f"PUSH CHANGE {attr.name} {attr.value} Q:{attr.quality}"
        )
