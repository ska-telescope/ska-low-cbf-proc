# Low.CBF Processor Tango Device #
The Low.CBF Processor TANGO device is used to control & monitor an Alveo FPGA card.

## Documentation
[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-low-cbf-proc/badge/?version=latest)](https://developer.skao.int/projects/ska-low-cbf-proc/en/latest/?badge=latest)

The documentation for this project can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-low-cbf-proc documentation](https://developer.skatelescope.org/projects/ska-low-cbf-proc/en/latest/index.html "SKA Developer Portal: ska-low-cbf-proc documentation")

## Project Avatar (Repository Icon)
[Processor icons created by Freepik - Flaticon](https://www.flaticon.com/free-icons/processor "processor icons")

# Directory Structure
Source code files are divided into subdirectories:
 - src/ska\_low\_cbf\_proc - module root - utility / miscellaneous
    - icl - Instrument Control (Abstraction) Layer common
      - correlator - Correlator personality ICL
      - pst - PulSar Timing personality ICL
    - processor - Processor Tango device

# Containers

- `pytango-xrt` - base image with Tango & XRT
  - Tagged with versions `<pytango>-<xrt>`, e.g. `9.5.0-202220.2.14.354`
  - Stored in our GitLab container registry
  - This container only needs to be rebuilt hen updating Tango, Ubuntu, or XRT version.
    Modify its Dockerfile and/or build script then run:
  ```shell
  GITLAB_USERNAME="<your username>" scripts/build-push-pytango-xrt-image.sh
  ```
- `ska-low-cbf-proc-xrt` - uses `pytango-xrt` and adds our Processor device code

For future use:
- `ska-low-cbf-proc-ami` - for new FPGA card using AMI interface

# Changelog

See [CHANGELOG.md](CHANGELOG.md)
